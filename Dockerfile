FROM eclipse-temurin:21-jre-noble
RUN apt-get update && \
apt-get install -y libxml2-utils && \
apt-get autoremove -y && \
apt-get clean \
ADD ead.xsd /app/ead.xsd
ADD apeEAD.xsd /app/apeEAD.xsd
CMD java -jar /app/app.jar



