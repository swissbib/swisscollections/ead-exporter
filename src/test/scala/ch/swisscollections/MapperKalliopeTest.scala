package ch.swisscollections

import ch.swisscollections.mapper.MapperKalliope
import org.scalactic.Explicitly.after
import org.scalatest.StreamlinedXml.streamlined
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}

import scala.io.Source
import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml.{Elem, Node, NodeSeq, XML}


class MapperKalliopeTest extends AnyFunSuite with AppSettings{

  private lazy val client = getSolrTestClient
  val mapperKalliope = new MapperKalliope

  def loadResult (filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/result/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }

  def loadInput (filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/record/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }

  def getSolrTestClient: SolrJClientWrapper = {

    val solrWrapper = SolrJClientWrapper(
      solrCollection,
      connectionTimeout,
      socketTimeout,
      nodeURL
    )
    solrWrapper
  }

  def removeSynchronisationDate(record: Elem): Elem = {
    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "date" && elem.attribute("type").head.text == "Synchronisierungsdatum" => NodeSeq.Empty
        case n => n
      }
    }
    val resultNodeSeq = new RuleTransformer(verbBody).transform(record)
    //todo : there should be a smarter way to do this
    XML.loadString(resultNodeSeq.toString())
  }

  test("Full Mapping Test") {
    val filename = "991170524809305501.xml"
    val record = new MockVuFindSolrRecord(filename)
    val result = mapperKalliope.mapIndividualRecord(record)
    removeSynchronisationDate(result) should equal(loadResult(filename))(after being streamlined[Elem])
  }

  //needs to fix synchronisation date problem
  test("Full Mapping Test 2") {
    val filename = "991170524810605501.xml"
    val record = new MockVuFindSolrRecord(filename)
    val result = mapperKalliope.mapIndividualRecord(record)
    removeSynchronisationDate(result) should equal(loadResult(filename))(after being streamlined[Elem])
  }

  test("simpleTag") {

    val record = new MockVuFindSolrRecord("991170524809305501.xml")
    val result = mapperKalliope.simpleTag(record, "852j", "unitid")
    assert(result == <unitid>UBH NL 136 : 2 f</unitid>)

    val result2 = mapperKalliope.simpleTag(record, "852p", "unitid")
    assert(result2 == NodeSeq.Empty)

    val result3 = mapperKalliope.simpleTag(record, "8524_j", "unitid")
    assert(result3 == <unitid>UBH NL 136 : 2 f</unitid>)

    val result4 = mapperKalliope.simpleTag(record, "655_7a", "test")
    assert(result4 == <test>Handschrift</test>)

    val result5 = mapperKalliope.simpleTag(record, "655E7a", "test")
    assert(result5 == NodeSeq.Empty)

    val result6 = mapperKalliope.simpleTagWithAttributes(record, "8524_j", "unitid", "label", "Weitere Signatur")
    val expected = <unitid label="Weitere Signatur">UBH NL 136 : 2 f</unitid>
    assert(result6 == expected)

    val record2 = new MockVuFindSolrRecord("991170525276405501.xml")
    val result7 = mapperKalliope.simpleTag(record2, "348a", "materialspec")
    assert(result7 == <materialspec>Chorbuch</materialspec>)

    val record3 = new MockVuFindSolrRecord("991170504975405501.xml")
    val result8 = {mapperKalliope.simpleTagWithAttributes(record3, "250a", "physfacet", "label", "Ausreifungsgrad")}
    assert(result8 == <physfacet label="Ausreifungsgrad">Kopie</physfacet>)

    val record5 = new MockVuFindSolrRecord("991170508842105501.xml")
    val result10 = <note label="Bemerkung" audience="external">      {mapperKalliope.simpleTag(record5, "500a", "p")}    </note>
    //println(result10)

  }

  test ("concat fields") {
    val record = new MockVuFindSolrRecord("991170351091005501.xml")
    val extent = {mapperKalliope.simpleTagConcat(record, "300a", "extent", ", ")}
    assert(extent == <extent>Partitur (314 S.) ; 42 cm, Chorpartitur (56 Bl.) ; 30 cm, Orchesterstimmen (ohne Perkussion) (Kopien) (47 Bl.), Stimmen (42 Bl.) ; 30 cm, Stimmen (10 Bl.) ; 30 cm, Stimmen (15 Bl.) ; 30 cm, Material (242 Bl.), 2 Musikkassetten, 2 Videokassetten</extent>)

    val record2 = new MockVuFindSolrRecord("991066266239705501.xml")
    val physfacet = {mapperKalliope.simpleTagWithAttributesConcat(record2, "340a", "physfacet", "label", "Material",", ")}
    assert(physfacet == <physfacet label="Material">Diarahmen, 5 x 5 cm</physfacet>)

  }

  test ("physdesc") {
    val record = new MockVuFindSolrRecord("991066266239705501.xml")
    val result = mapperKalliope.getPhysDesc(record)
    val resultElem = XML.loadString(result.toString())
    resultElem should equal (loadResult("physdesc.xml")) (after being streamlined[Elem])
  }

  test ("unit title") {
    val record = new MockVuFindSolrRecord("991170461581205501.xml")
    val titleWithAuthor = {mapperKalliope.getUnitTitle(record)}
    assert(titleWithAuthor == "Brief an Paula Häberlin / von Jacqueline Ziegler")

    val record2 = new MockVuFindSolrRecord("991066266239705501.xml")
    val titleWithSub = {mapperKalliope.getUnitTitle(record2)}
    assert(titleWithSub == "[Der Schlot der Fabrik Robert Schwarzenbach & Co. wird für die Sprengung vorbereitet] : Untertitel / [Paul Strebel]")

    val record3 = new MockVuFindSolrRecord("991170609528205501.xml")
    val result3 = {mapperKalliope.getUnitTitleAlt(record3)}
    val resultElem = XML.loadString(result3.toString())

    resultElem should equal (loadResult("unittitle-alt.xml")) (after being streamlined[Elem])

  }

  test ("dates") {
    val record = new MockVuFindSolrRecord("991170525276405501.xml")
    val creationDate = {mapperKalliope.getCreateDate(record)}
    assert(creationDate == <date type="Erfassungsdatum" normal="20181016">16.10.2018</date>)
    val modificationDate = {mapperKalliope.getModificationDate(record)}
    assert(modificationDate == <date type="Modifikationsdatum" normal="20211215">15.12.2021</date>)

    val record2 = new MockVuFindSolrRecord("991170508842105501.xml")
    val date = {mapperKalliope.getUnitDate(record2)}
    assert(date == <unitdate normal="19520925/19521029">25.09.1952-29.10.1952</unitdate>)

    val record3 = new MockVuFindSolrRecord("991170504975405501.xml")
    val date3 = {mapperKalliope.getUnitDate(record3)}
    assert(date3 == <unitdate normal="1949/1970">1949-1970</unitdate>)

    val record4 = new MockVuFindSolrRecord("991066266239705501.xml")
    val date4 = {mapperKalliope.getUnitDate(record4)}
    assert(date4 == <unitdate normal="1983">[1983]</unitdate>)

    val record5 = new MockVuFindSolrRecord("991170522836405501.xml")
    val dateBC = {mapperKalliope.getUnitDate(record5)}
    assert(dateBC == <unitdate normal="-0550/0599">2. Hälfte 6. Jahrhundert</unitdate>)

    val record6 = new MockVuFindSolrRecord("991170609528205501.xml")
    val date6 = {mapperKalliope.getUnitDate(record6)}
    assert(date6 == <unitdate normal="1500/1599">[15--]</unitdate>)

    val record7 = new MockVuFindSolrRecord("991170499444605501.xml")
    val date7 = {mapperKalliope.getUnitDate(record7)}
    assert(date7 == <unitdate normal="1866-04">[1866]</unitdate>)

    val record8 = new MockVuFindSolrRecord("991170514773105501.xml")
    val date8 = {mapperKalliope.getUnitDate(record8)}
    assert(date8 == <unitdate>19uu</unitdate>)

    val record9 = new MockVuFindSolrRecord("991170467343005501.xml")
    val date9 = {mapperKalliope.getCreateDate(record9)}
    assert(date9 == <date type="Erfassungsdatum">00.00.2000</date>)

    val record10 = new MockVuFindSolrRecord("991170461257105501.xml")
    val date10 = {mapperKalliope.getUnitDate(record10)}
    assert(date10 == <unitdate normal="1875-06/18800315">1875.06.00-1880.03.15</unitdate>)
  }

  test ("language") {
    val record = new MockVuFindSolrRecord("991047952279705501.xml")
    val language = {mapperKalliope.getLanguage(record)}
    val resultElem = XML.loadString(language.toString())
    resultElem should equal (loadResult("language.xml")) (after being streamlined[Elem])

    val record2 = new MockVuFindSolrRecord("991170351091005501.xml")
    val language2 = {mapperKalliope.getLanguage(record2)}
    assert(language2 == <langmaterial><language langcode="ger">Deutsch</language></langmaterial>)
  }

  test ("multiple fields") {
    val record = new MockVuFindSolrRecord("991170508842105501.xml")
    val result = {mapperKalliope.simpleTag(record, "509a", "extent")}
    val result10 = <note label="Bemerkung" audience="external">      {mapperKalliope.simpleTag(record, "520a", "p")}    </note>
    //println(result10)
  }

  test ("scopecontent") {
    val record = new MockVuFindSolrRecord("991170428275205501.xml")
    val result = mapperKalliope.getScopeContent(record)
    val resultElem = XML.loadString(result.toString())
    resultElem should equal (loadResult("scopecontent-596.xml")) (after being streamlined[Elem])


    val record2 = new MockVuFindSolrRecord("991170491486005501.xml")
    val result2 = mapperKalliope.getScopeContentAbstract(record2)
    val resultElem2 = XML.loadString(result2.toString())

    resultElem2 should equal (loadResult("scopecontent-520.xml")) (after being streamlined[Elem])
  }

  test ("userestrict") {
    val record = new MockVuFindSolrRecord("991170524810605501.xml")
    val result = mapperKalliope.getUserestrict(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("userestrict.xml")) (after being streamlined[Elem])
  }

  test ("arrangement") {
    val record = new MockVuFindSolrRecord("991170976552805501.xml")
    val result = mapperKalliope.getArrangement(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("arrangement.xml")) (after being streamlined[Elem])
  }

  test ("bibliography") {
    val record = new MockVuFindSolrRecord("991069533649705501.xml")
    val result = mapperKalliope.getBibliographyReference(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("bibliography.xml")) (after being streamlined[Elem])

    val record2 = new MockVuFindSolrRecord("991170525276405501.xml")
    val result2 = mapperKalliope.getBibliographyReference(record2)
    val result3 = mapperKalliope.getBibliographyPublication(record2)
    val resultElem3 = XML.loadString(result3.toString())

    resultElem3 should equal (loadResult("bibliography-publication.xml")) (after being streamlined[Elem])

    assert(result2 == NodeSeq.Empty)

  }

  test ("acqinfo") {
    val record = new MockVuFindSolrRecord("991170430279905501.xml")
    val result = mapperKalliope.getAcqinfo(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("acqinfo.xml")) (after being streamlined[Elem])
  }

  test ("related material") {
    val record = new MockVuFindSolrRecord("991170430279905501.xml")
    val result = mapperKalliope.getRelatedMaterial(record)
    val resultElem = XML.loadString(result.toString())
    //println(result)

    resultElem should equal (loadResult("relatedMaterial.xml")) (after being streamlined[Elem])
  }

  test ("former call number") {
    val record = new MockVuFindSolrRecord("991170428275205501.xml")
    val result = mapperKalliope.getFormerCallNumber(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("formerCallNumber.xml")) (after being streamlined[Elem])
  }

  test ("origination") {
    val record = new MockVuFindSolrRecord("991170427384605501.xml")
    val result = mapperKalliope.getOriginationPerson(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("origination.xml")) (after being streamlined[Elem])
  }

  test ("controlaccess") {
    val record = new MockVuFindSolrRecord("991170427384605501.xml")
    val result = mapperKalliope.getPersonControlAccess(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("controlaccess-person.xml")) (after being streamlined[Elem])

    val result2 = mapperKalliope.getCorporateControlAccess(record)
    val resultElem2 = XML.loadString(result2.toString())

    resultElem2 should equal (loadResult("controlaccess-corporate.xml")) (after being streamlined[Elem])
  }

  test ("subjects") {
    val record = new MockVuFindSolrRecord("991047952279705501.xml")
    val result = mapperKalliope.getSubjectControlAccess(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("controlaccess-subject.xml")) (after being streamlined[Elem])

    val result2 = mapperKalliope.getGenreControlAccess(record)
    val resultElem2 = XML.loadString(result2.toString())

    resultElem2 should equal (loadResult("controlaccess-genre.xml")) (after being streamlined[Elem])

  }

  test("Full record ZB collections") {
    val filename = "ZBC471ed9ffe8884eefa57bc00601f9f770.xml"
    val record = new MockVuFindSolrRecord(filename)
    val result = mapperKalliope.mapIndividualRecord(record)
    removeSynchronisationDate(result) should equal(loadResult(filename))(after being streamlined[Elem])
  }



}
