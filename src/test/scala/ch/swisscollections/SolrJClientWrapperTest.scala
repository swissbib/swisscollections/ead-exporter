package ch.swisscollections


import org.scalatest.funsuite.AnyFunSuite

import org.scalatest.Tag

object NeedsSolrConnection extends Tag("ch.swisscollections.tags.NeedsSolrConnection")

import scala.jdk.CollectionConverters._

import scala.io.Source

class SolrJClientWrapperTest extends AnyFunSuite with AppSettings {

  private lazy val client = getSolrTestClient

  def getSolrTestClient: SolrJClientWrapper = {
    val solrWrapper = SolrJClientWrapper(
      solrCollection,
      connectionTimeout,
      socketTimeout,
      nodeURL)
    solrWrapper
  }


  //needs a solr cluster available to test this or we should mock it
  test("Get Children", NeedsSolrConnection) {
    val results = client.getChildren("991170524810605501", "")
    assert(results.size == 6)

    val results2 = client.getChildren("991170431759505501", "")
  }

  //needs a solr cluster available to test this or we should mock it
  test("Get All archives", NeedsSolrConnection) {
    val results = client.getAllArchivesIdentifiers("localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A100")
    assert(results.size > 400)
    assert(results.size < 600)
    assert(results.contains("991170430545505501"))
    assert(results.contains("991170430427005501"))
    assert(results.contains("991170430298005501"))
    assert(results.contains("991170430612805501"))
  }

  //needs a solr cluster available to test this or we should mock it
  test("Get All pseudo archives", NeedsSolrConnection) {
    val queryString = "hierarchy_top_id:991170430282105501 OR hierarchy_top_id:991170430284105501"
    val results0 = client.getAllPseudoArchivesIdentifiers(queryString, List())
    val results1 = client.getAllPseudoArchivesIdentifiers(queryString, List("991170430282105501"))
    val results2 = client.getAllPseudoArchivesIdentifiers(queryString, List("991170430282105501","991170430284105501"))

    assert(results2.size == 0)
    assert(results1.size < results0.size)

  }

}
