package ch.swisscollections

import ch.swisscollections.App.{almaIdentifierForPseudoArchives, buildAndWritePseudoArchive, getListOfFiles, zip}
import org.scalactic.Explicitly.after
import org.scalatest.StreamlinedXml.streamlined
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}

import java.io.{File, PrintWriter}
import scala.io.Source
import scala.xml.{Elem, Node, PrettyPrinter, XML}
import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml.{Elem, Node, NodeSeq, XML}
import scala.xml.transform.RewriteRule


class AppTest extends AnyFunSuite with AppSettings {

  private lazy val client = getSolrTestClient

  def getSolrTestClient: SolrJClientWrapper = {
    val solrWrapper = SolrJClientWrapper(
      solrCollection,
      connectionTimeout,
      socketTimeout,
      nodeURL)
    solrWrapper
  }

  def removeSynchronisationDate(record: Elem): Elem = {
    val verbBody: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case elem: Elem if elem.label == "date" && elem.attribute("type").head.text == "Synchronisierungsdatum" => NodeSeq.Empty
        case elem: Elem if elem.label == "date" && elem.attribute("type").head.text == "Modifikationsdatum" => NodeSeq.Empty
        case n => n
      }
    }
    val resultNodeSeq = new RuleTransformer(verbBody).transform(record)
    //todo : there should be a smarter way to do this
    XML.loadString(resultNodeSeq.toString())
  }

  def loadResult(filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/result/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }

  ignore("zipping files") {
    val filesList = getListOfFiles("/home/lionel/Data/arbim/swisscollections/export/temp/ape/2023-11-27-17-58/Z01/").map(file => file.getPath)

    if (filesList.length > 0) {
      //zip archive pro institution
      zip("/home/lionel/Bureau/test.zip", filesList)
    }
    println(filesList)
  }

  //Tests for exclusion Alther (991170430909405501) should not contain 991170472851905501, 16025 hits
  // Geigy-Hagenbach (991170430677505501) should not throw errors and not contain 991171042604905501
  test("Build tree", NeedsSolrConnection) {
    val record = client.getById("991170430401805501")
    val institution = "A100"
    val exclusionCriteria = exclusionCriterionsForArchives(institution)
    val result = App.buildTree(record, exclusionCriteria)
    val resultAsXml = scala.xml.XML.loadString(result)
    val p = new PrettyPrinter(80, 4)
    removeSynchronisationDate(resultAsXml) should equal(loadResult("buildTree.xml"))(after being streamlined[Elem])

    //val pw = new PrintWriter(new File("src/test/resources/result/geigy-hagenbach-neu.xml"))
    //pw.write(p.format(scala.xml.XML.loadString(result)))
    //pw.close
  }

  ignore("pseudo archives") {
    val pseudoArchivesIdentifiers: List[String] = List("991170832178005501", "991170832173905501")
    val institution = "A100"
    buildAndWritePseudoArchive(pseudoArchivesIdentifiers, institution, "/home/lionel/Documents/mycloud/arbim/git_repo/swisscollections/ead-exporter/src/test")
  }

  //this test can not work on gitlab as xmllint is not available in the sbt container
  //but it is important to run it locally
  ignore("get number of invalid valid files ") {

    val PATH = getClass.getResource("").getPath + "/../../"
    val files = List(
      PATH + "result/thurgau-EAD.xml", //valid ead
      PATH + "record/991047952279705501.xml" //other xml
    )
    val xsd = PATH + "xsd-schemas/eadTest.xsd"

    val (numberOfInvalidFiles, output) = App.getNumberOfInvalidFiles(files, xsd)

    assert(numberOfInvalidFiles == 1)
    assert(output.contains("thurgau-EAD.xml validates"))
    assert(output.contains("991047952279705501.xml fails to validate"))

    //now for ape xsd
    val filesAPE = List(
      PATH + "result/thurgau-APE-EAD.xml", //valid APE ead
      PATH + "result/thurgau-EAD.xml" //kalliope xml
    )
    val xsdAPE = PATH + "xsd-schemas/apeEADTest.xsd"

    val (numberOfInvalidFilesApe, outputApe) = App.getNumberOfInvalidFiles(filesAPE, xsdAPE)

    assert(numberOfInvalidFilesApe == 1)

    assert(outputApe.contains("thurgau-APE-EAD.xml validates"))
    assert(outputApe.contains("thurgau-EAD.xml fails to validate"))
  }
}
