package ch.swisscollections

import org.scalatest.funsuite.AnyFunSuite

import scala.jdk.CollectionConverters._

class VuFindSolrRecordTest extends AnyFunSuite with AppSettings {

  private lazy val client = getSolrTestClient

  def getSolrTestClient: SolrJClientWrapper = {

    val solrWrapper = SolrJClientWrapper(
      solrCollection,
      connectionTimeout,
      socketTimeout,
      nodeURL
    )
    solrWrapper
  }

  test("Test Get Subfield / Controlfield ...") {
    val record = new MockVuFindSolrRecord(("991170524810605501.xml"))

    val controlfield001 = record.getControlfield("001")
    assert(controlfield001 == "991170524810605501")

    // with ind1

    val field8524_p = record.getFirstSubfield("852","j", "4", " ").getOrElse("")
    assert(field8524_p == "UBH NL 136 : 2")

    //without ind1
    val field852__p = record.getSubfields("852","j").getOrElse(List())
    assert(field852__p.head == "UBH NL 136 : 2")

    //with other ind1
    val field8525_p = record.getSubfields("852","j", "5", " ")
    assert(field8525_p == None)

  }
  test("Get Field") {
    val record = new MockVuFindSolrRecord("991170508842105501.xml")
    val fields700 = record.getFieldsAsMap("700")
    assert (fields700(0)("a") == "Sabais, Heinz Winfried")
    assert (fields700(1)("a") == "Burckhardt, Carl Jacob")
    assert (fields700(1)("d") == "1891-1974")
    assert (fields700(1).getOrElse("w","dummy text") == "dummy text")
  }

}
