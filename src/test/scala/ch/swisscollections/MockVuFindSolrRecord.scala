package ch.swisscollections

import ch.swisscollections.mapper.VuFindSolrRecord
import org.apache.solr.common.SolrDocument

import scala.io.Source
import scala.xml.XML


/**
 * Instead of loading a record from solr, we load it directly from a file within the test/resources/record folder
 * The id is taken from 001
 * If needed later, we could pass a map as a parameter, with the solr fields
 *
 * @param filename the file
 * @param solrDocument
 */
class MockVuFindSolrRecord(filename: String, solrDocument: SolrDocument) extends VuFindSolrRecord(solrDocument: SolrDocument) {

  override def xmlFullRecord = XML.loadString(getRecordFromRecordFolder(filename))

  setField("id", getControlfield("001"))

  /**
   * For this auxiliary constructor, we use an empty solrDocument
   * @param filename
   */
  def this(filename: String) = this(filename, new SolrDocument())

  private def getRecordFromRecordFolder(filename: String): String = {
    val file = Source.fromFile(s"src/test/resources/record/$filename")
    val text = file.mkString
    file.close()
    text
  }


}
