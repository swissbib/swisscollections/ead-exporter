package ch.swisscollections

import ch.swisscollections.mapper.MapperApe
import org.apache.solr.common.{SolrDocument, SolrInputDocument}
import org.scalactic.Explicitly.after
import org.scalatest.StreamlinedXml.streamlined
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}

import scala.jdk.CollectionConverters._
import scala.io.Source
import scala.xml.{Elem, NodeSeq, XML}



class MapperApeTest extends AnyFunSuite with AppSettings{

  private lazy val client = getSolrTestClient
  val mapperApe = new MapperApe

  def loadResult (filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/result/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }

  def loadInput (filename: String): Elem = {
    val file = Source.fromFile(s"src/test/resources/record/$filename")
    val text = file.mkString
    val result: Elem = XML.loadString(text)
    result
  }

  def getSolrTestClient: SolrJClientWrapper = {

    val solrWrapper = SolrJClientWrapper(
      solrCollection,
      connectionTimeout,
      socketTimeout,
      nodeURL
    )
    solrWrapper
  }


  test("Full Mapping Test") {
    val filename = "991170524809305501.xml"
    val record = new MockVuFindSolrRecord(filename)
    val result = mapperApe.mapIndividualRecord(record)
    result should equal(loadResult("991170524809305501-ape.xml"))(after being streamlined[Elem])

  }

  test ("controlaccess") {
    val record = new MockVuFindSolrRecord("991170427384605501.xml")
    val result = mapperApe.getPersonControlAccess(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("controlaccess-person-ape.xml")) (after being streamlined[Elem])

    val result2 = mapperApe.getCorporateControlAccess(record)
    val resultElem2 = XML.loadString(result2.toString())

    resultElem2 should equal (loadResult("controlaccess-corporate-ape.xml")) (after being streamlined[Elem])
  }

  test ("subjects") {
    val record = new MockVuFindSolrRecord("991047952279705501.xml")
    val result = mapperApe.getSubjectControlAccess(record)
    val resultElem = XML.loadString(result.toString())

    resultElem should equal (loadResult("controlaccess-subject-ape.xml")) (after being streamlined[Elem])

  }

  test ("places") {
    val record = new MockVuFindSolrRecord("991170491486005501.xml")
    val result = mapperApe.getPlaceControlAccess(record)
    println (result)
  }

  test ("multiple holdings") {
    val record = new MockVuFindSolrRecord("991170513318905501.xml")
    val value = {mapperApe.getLibraryName(record)}
    assert(value == "Basel UB")
  }

  test ("ape links") {
    val record = new MockVuFindSolrRecord("991170478369105501.xml")
    val result = {mapperApe.getObjectLinks(record)}
    assert(result.toString() == "<dao xlink:href=\"http://dx.doi.org/10.7891/e-manuscripta-100061\" xlink:title=\"Digitalisat\"/>")

    val thumbnailLink = {mapperApe.getEmanuscriptaThumbnail(record)}
    assert(thumbnailLink.toString() == "<dao xlink:href=\"https://www.e-manuscripta.ch/titlepage/doi/10.7891/e-manuscripta-100061\" xlink:title=\"thumbnail\"/>")

    val findaidLink = {mapperApe.getFindaidLinks(record)}
    assert(findaidLink.toString() == "<otherfindaid><p><extref xlink:href=\"https://hls-dhs-dss.ch/de/articles/011675/2013-12-05/\" xlink:title=\"Biografie im Historischen Lexikon der Schweiz\"/></p></otherfindaid>")
  }

}
