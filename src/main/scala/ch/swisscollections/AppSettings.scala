package ch.swisscollections

trait AppSettings {

  val nodeURL: String = sys.env.getOrElse("NODE_URL", "https://solrtest.swisscollections.unibas.ch/solr/")
  val solrCollection: String = sys.env.getOrElse("SOLR_COLLECTION", "swisscollectionstest")

  val connectionTimeout: String = sys.env.getOrElse("CONNECTION_TIMEOUT", "20000")
  val socketTimeout: String = sys.env.getOrElse("SOCKET_TIMEOUT", "60000")

  val exportFolder : String = sys.env.getOrElse("EXPORT_FOLDER", "/export/kalliope/")

  val tempFolder: String = sys.env.getOrElse("TEMP_FOLDER", "/export/temp/kalliope/")

  val exportType: String = sys.env.getOrElse("EXPORT_TYPE", "kalliope")

  //all EAD files will start with this
  val prefixFilename = "ead_CH-002121-2_"

  //this suffix will be added to pseudo-archives
  val suffixPseudoArchives = "_einzeldokumente"

  val maxChildRecords : Int = sys.env.get("MAX_CHILD_RECORDS") match {
    case Some(s) => s.toInt
    case None => 1000
  }

  val maxArchivesProInstitution : Int = sys.env.get("MAX_ARCHIVES_PRO_INSTITUTION") match {
    case Some(s) => s.toInt
    case None => 10000
  }


  val maxPseudoArchivesProInstitution: Int = sys.env.get("MAX_PSEUDO_ARCHIVES_PRO_INSTITUTION") match {
    case Some(s) => s.toInt
    case None => 175000
  }
  val mailServer : String = sys.env.getOrElse("MAIL_SERVER", "authsmtp.securemail.pro")

  val mailServerPort : Int = sys.env.get("MAIL_SERVER_PORT") match {
    case Some(s) => s.toInt
    case None => 465
  }

  val emailAccount : String = sys.env.getOrElse("EMAIL_ACCOUNT", "info@swisscollections.ch")

  val emailPassword : String = sys.env.getOrElse("EMAIL_PASSWORD", "")

  val mailFrom : String = sys.env.getOrElse("MAIL_FROM", "info@swisscollections.ch")

  val mailTo: String = sys.env.getOrElse("MAIL_TO", "lionel.walter@arbim.ch")

  val mailCc: String = sys.env.getOrElse("MAIL_CC", "lionel.walter@arbim.ch")

  val downloadUrl : String = sys.env.getOrElse("DOWNLOAD_URL", "localhost")

  val eadXsdLocation: String = sys.env.getOrElse("EAD_XSD_LOCATION", "ead.xsd")

  //See https://ub-basel.atlassian.net/wiki/spaces/RH/pages/1974861825/Selektionskriterien+EAD+Exportq
  val searchCriterionsForArchivesKalliope: Map[String, String] = Map(
    "B583RO"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:B583RO",
    "SGARK"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:SGARK",
    "SGKBV"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:SGKBV NOT localcode:\"HANcollect_this handschrift\"",
    "A125"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A125",
    "AKB"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:AKB",
    "A381"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A381",
    "SGSTI"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:SGSTI",
    "A100"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A100 NOT (localcode:\"HANcollect_this handschrift\" OR localcode:\" HANcollect_this miszellan\")",
    "A150"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A150",
    "LUZHB"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:LUZHB NOT localcode:\"HANcollect_this handschrift\"",
    "B415"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:B415",
    "B400"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND (institution:B400 OR institution:B404) NOT id:991170538907505501",
    "A382"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A382",
    "Z01"
      -> "id:ZBC* AND description_level_name_str_mv:bestand",
  )

  val searchCriterionsForArchivesApe: Map[String, String] = Map(
    "B583RO"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:B583RO",
    "SGARK"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:SGARK",
    "SGKBV"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:SGKBV NOT localcode:\"HANcollect_this handschrift\"",
    "A125Pers"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A125 AND localcode:swapapnl AND NOT (localcode:swapafirma OR localcode:swapaverband)",
    "A125Corp"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A125 AND (localcode:swapafirma OR localcode:swapaverband)",
    "AKB"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:AKB",
    "A381"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A381",
    "SGSTI"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:SGSTI",
    "A100"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A100 NOT (localcode:\"HANcollect_this handschrift\" OR localcode:\" HANcollect_this miszellan\")",
    "A150"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A150",
    "LUZHB"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:LUZHB NOT localcode:\"HANcollect_this handschrift\"",
    "B415"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:B415",
    "B400"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND (institution:B400 OR institution:B404) NOT id:991170538907505501",
    "A382"
      -> "localcode:HANunikat AND description_level_name_str_mv:bestand AND institution:A382",
    "Z01"
      -> "id:ZBC* AND description_level_name_str_mv:bestand",
  )

  val searchCriterionsForPseudoArchivesKalliope: Map[String, String] = Map(
    "B583RO"
      -> "localcode:HANunikat AND institution:B583RO NOT description_level_name_str_mv:bestand",
    "SGARK"
      -> "localcode:HANunikat AND institution:SGARK NOT description_level_name_str_mv:bestand",
    "SGKBV"
      -> "localcode:HANunikat AND institution:SGKBV NOT description_level_name_str_mv:bestand NOT localcode:\"HANcollect_this handschrift\"",
    "A125"
      -> "localcode:HANunikat AND institution:A125 NOT description_level_name_str_mv:bestand",
    "AKB"
      -> "localcode:HANunikat AND institution:AKB NOT description_level_name_str_mv:bestand",
    "A381"
      -> "localcode:HANunikat AND institution:A381 NOT description_level_name_str_mv:bestand",
    "SGSTI"
      -> "localcode:HANunikat AND institution:SGSTI NOT description_level_name_str_mv:bestand",
    "A100"
      -> "localcode:HANunikat AND institution:A100 NOT description_level_name_str_mv:bestand NOT (localcode:\"HANcollect_this handschrift\" OR localcode:\"HANcollect_this miszellan\" OR description_level_name_str_mv:hauptabteilung OR description_level_name_str_mv:abteilung)",
    "A150"
      -> "localcode:HANunikat AND institution:A150 NOT description_level_name_str_mv:bestand",
    "LUZHB"
      -> "localcode:HANunikat AND institution:LUZHB NOT description_level_name_str_mv:bestand NOT localcode:\"HANcollect_this handschrift\"",
    "B415"
      -> "localcode:HANunikat AND institution:B415 NOT description_level_name_str_mv:bestand",
    "B400"
      -> "localcode:HANunikat AND (institution:B400 OR institution:B404) NOT description_level_name_str_mv:bestand NOT hierarchy_top_id:991170538907505501",
    "A382"
      -> "localcode:HANunikat AND institution:A382 NOT description_level_name_str_mv:bestand",
    "Z01"
      -> "ctrlnum:NEBIS*EBI04 OR ((subform_gnd:handschrift AND subform_gnd:briefsammlung) AND institution:Z03 NOT id:ZBC*)",
  )

  val searchCriterionsForPseudoArchivesApe: Map[String, String] = Map(
    "B583RO"
      -> "localcode:HANunikat AND institution:B583RO NOT description_level_name_str_mv:bestand",
    "SGARK"
      -> "localcode:HANunikat AND institution:SGARK NOT description_level_name_str_mv:bestand",
    "SGKBV"
      -> "localcode:HANunikat AND institution:SGKBV NOT description_level_name_str_mv:bestand NOT localcode:\"HANcollect_this handschrift\"",
    "A125Pers"
      -> "institution:A125 NOT institution:A125", // goal is to have 0 hits
    "A125Corp"
      -> "institution:A125 NOT institution:A125", // goal is to have 0 hits
    "AKB"
      -> "localcode:HANunikat AND institution:AKB NOT description_level_name_str_mv:bestand",
    "A381"
      -> "localcode:HANunikat AND institution:A381 NOT description_level_name_str_mv:bestand",
    "SGSTI"
      -> "localcode:HANunikat AND institution:SGSTI NOT description_level_name_str_mv:bestand",
    "A100"
      -> "localcode:HANunikat AND institution:A100 NOT description_level_name_str_mv:bestand NOT (localcode:\"HANcollect_this handschrift\" OR localcode:\"HANcollect_this miszellan\" OR description_level_name_str_mv:hauptabteilung OR description_level_name_str_mv:abteilung)",
    "A150"
      -> "localcode:HANunikat AND institution:A150 NOT description_level_name_str_mv:bestand",
    "LUZHB"
      -> "localcode:HANunikat AND institution:LUZHB NOT description_level_name_str_mv:bestand NOT localcode:\"HANcollect_this handschrift\"",
    "B415"
      -> "localcode:HANunikat AND institution:B415 NOT description_level_name_str_mv:bestand",
    "B400"
      -> "localcode:HANunikat AND (institution:B400 OR institution:B404) NOT description_level_name_str_mv:bestand NOT hierarchy_top_id:991170538907505501",
    "A382"
      -> "localcode:HANunikat AND institution:A382 NOT description_level_name_str_mv:bestand",
    "Z01"
      -> "ctrlnum:NEBIS*EBI04 OR ((subform_gnd:handschrift AND subform_gnd:briefsammlung) AND institution:Z03 NOT id:ZBC*)",
  )

  val exclusionCriterionsForArchives: Map[String, String] = Map(
    "B583RO"
      -> "",
    "SGARK"
      -> "",
    "SGKBV"
      -> " NOT localcode:\"HANcollect_this handschrift\"",
    "A125"
      -> "",
    "A125Pers"
      -> "",
    "A125Corp"
      -> "",
    "AKB"
      -> "",
    "A381"
      -> "",
    "SGSTI"
      -> "",
    "A100"
      -> " AND institution:* NOT (localcode:\"HANcollect_this handschrift\" OR localcode:\" HANcollect_this miszellan\") NOT location_str_mv:118B1",
    "A150"
      -> "",
    "LUZHB"
      -> " NOT localcode:\"HANcollect_this handschrift\"",
    "B415"
      -> "",
    "B400"
      -> "",
    "A382"
      -> "",
    "Z01"
      -> "",
  )

  val filenamesProInstitution: Map[String, String] = Map(
    "B583RO" -> "Bern-UB-Medizingeschichte-Rorschach-Archiv-CH-000956-2",
    "SGARK" -> "KB-Appenzell-Ausserrhoden-CH-000095-1",
    "SGKBV" -> "St-Gallen-KB-Vadiana-CH-000009-3",
    "A125" -> "Basel-UB-Wirtschaft-SWA-CH-000133-4",
    "A125Pers" -> "Basel-UB-Wirtschaft-SWA-Personen-CH-000133-4",
    "A125Corp" -> "Basel-UB-Wirtschaft-SWA-Koerperschaften-CH-000133-4",
    "AKB" -> "KB-Aargau-CH-000050-X",
    "A381" -> "KB-Thurgau-CH-000086-2",
    "SGSTI" -> "St-Gallen-Stiftsbibliothek-CH-000093-7",
    "A100" -> "Basel-UB-CH-000004-7",
    "A150" -> "Solothurn-ZB-CH-000045-X",
    "LUZHB" -> "Luzern-ZHB-CH-000006-1",
    "B415" -> "Bern-UB-Schweizerische-Osteuropabibliothek-CH-000284-9",
    "B400" -> "Bern-UB-Bibliothek-Muenstergasse-CH-000011-1",
    "A382" -> "Zofingen-SB-CH-000048-1",
    "Z01" -> "ZB-Zuerich-CH-000008-6",
  )

  val almaIdentifierForPseudoArchives: Map[String, String] = Map(
    "B583RO" -> "000", //not used
    "A100" -> "991170432022405501",
    "A125" -> "991170432022305501",
    "A125Pers" -> "000", //not used
    "A125Corp" -> "991170432022305501",
    "SGARK" -> "991170432022105501",
    "A381" -> "991170432144305501",
    "AKB" -> "991170432284305501",
    "LUZHB" -> "991170350964205501",
    "A150" -> "991170427534505501",
    "SGKBV" -> "991170432144205501",
    "SGSTI" -> "991170432144105501",
    "B415" -> "991170538915205501",
    "B400" -> "991170538913205501",
    "A382" -> "991170432284205501",
    "Z01" -> "881170715577605501",
  )
}
