package ch.swisscollections

import ch.swisscollections.mapper.VuFindSolrRecord
import org.apache.logging.log4j.scala.Logging
import org.apache.solr.client.solrj.SolrRequest.METHOD
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.client.solrj.{SolrClient, SolrQuery}
import org.apache.solr.common.SolrDocument

import scala.jdk.CollectionConverters._


class SolrJClientWrapper private (client:SolrClient,
                                  collection:String,
                                 ) extends Logging with AppSettings {

  /**
   * Get the children of a given document
   * @param id the id of the parent
   * @return the list of the children
   */
  def getChildren(id:String, exclusionCriteria: String): List[SolrDocument] = {
    val solrQuery = new SolrQuery
    solrQuery.set("q", "hierarchy_parent_id:" + id + exclusionCriteria)
    solrQuery.set("sort", "hierarchy_sequence asc")
    //we assume that a record don't have more than 1000 children
    solrQuery.set("rows", maxChildRecords)
    val res: List[SolrDocument] = client.query(collection, solrQuery).getResults.asScala.toList
    res
  }

  /**
   * Get a record by id
   * @param id the id
   * @return the record
   */
  def getById(id:String): VuFindSolrRecord = {
    val record = new VuFindSolrRecord(client.getById(collection, id))
    record
  }

  /**
   * Get All Archives Identifiers
   * @param institutionQuery the solr query to identify all archives
   * @return a list with the identifiers of all archives
   */
  def getAllArchivesIdentifiers(institutionQuery: String): List[String] = {
    val solrQuery = new SolrQuery
    solrQuery.set("q", institutionQuery)
    solrQuery.set("fl", "id")
    solrQuery.set("rows", maxArchivesProInstitution)
    val res: List[SolrDocument] = client.query(collection, solrQuery).getResults.asScala.toList
    val identifiers = res.map{ v => v.get("id").toString }
    identifiers
  }


  /**
   * Get the identifiers of all records to attach to the pseudo-archive
   * @param institutionQuery the solr query for pseudo archives
   * @param hierarchyTopIdsToIgnore the identifiers of the top nodes of archives for this institution. The records which are part of one of
   *                                these archives will be ignored
   * @return the identifiers of all records to attach to the pseudo-archive
   */
  def getAllPseudoArchivesIdentifiers(institutionQuery: String, hierarchyTopIdsToIgnore: List[String]): List[String] = {
    val solrQuery = new SolrQuery

    val excludeQuery = hierarchyTopIdsToIgnore.mkString(" NOT hierarchy_top_id:")
    if(hierarchyTopIdsToIgnore.nonEmpty) {
      solrQuery.set("q", institutionQuery + " NOT hierarchy_top_id:" + excludeQuery)
    } else {
      solrQuery.set("q", institutionQuery)
    }
    solrQuery.set("fl", "id")
    //todo move 10000 to the configuration. It will only export the first 5000 archives.
    //we assume that no institution have more than 100000 pseudo archives
    solrQuery.set("rows", maxPseudoArchivesProInstitution)
    //need to do a post request as this query is very long
    val res: List[SolrDocument] = client.query(collection, solrQuery, METHOD.POST).getResults.asScala.toList
    val identifiers = res.map{ v => v.get("id").toString }
    identifiers
  }
}


object SolrJClientWrapper {
  def apply(collection: String,
            connectionTimeout: String, socketTimeout: String, nodeURL: String): SolrJClientWrapper = {
    val client = new HttpSolrClient.Builder(nodeURL)
      .withConnectionTimeout(connectionTimeout.toInt)
      .withSocketTimeout(socketTimeout.toInt)
      .build()
    new SolrJClientWrapper(client,collection)

  }

}
