package ch.swisscollections

import ch.swisscollections.mapper.{MapperApe, MapperKalliope, VuFindSolrRecord}
import courier.Defaults.executionContext
import courier._
import org.apache.logging.log4j.scala.Logging
import org.apache.solr.common.SolrDocument
import org.joda.time.DateTime

import java.io._
import java.util.zip.{ZipEntry, ZipOutputStream}

import java.nio.file.{Paths, Path}

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.reflect.io.Directory
import scala.sys.process._
import scala.util.{Failure, Success, Try}
import scala.xml.PrettyPrinter

object App extends scala.App with Logging with AppSettings {
  private lazy val client = getSolrClient
  generateEadExport

  /**
   * This generates an Ead Export based on the environment variables (see Readme)
   */
  def generateEadExport: Unit = {
    var filesListAll = List[String]()

    val processEnd:DateTime = DateTime.now()
    val dateString = processEnd.toString("yyyy-MM-dd-HH-mm")

    val resultDirectory = new Directory(new File(exportFolder + '/' + dateString))
    resultDirectory.createDirectory()

    val currentTempFolder = tempFolder + '/' + dateString

    val searchCriterionsForArchives = exportType match {
      case "kalliope" => searchCriterionsForArchivesKalliope
      case "ape" => searchCriterionsForArchivesApe
      case _ => throw new RuntimeException("invalid export type, only kalliope or ape are valid values")
    }

    for((institution, searchQuery) <- searchCriterionsForArchives) {
      val topIdList = exportArchivesFromInstitution(institution, searchQuery, currentTempFolder)
      exportPseudoArchiveFromInstitution(institution, topIdList, currentTempFolder)



      val filesList = getListOfFiles(currentTempFolder + "/" + institution).map(file => file.getPath)
      if(filesList.length>0) {
        //zip archive pro institution
        filesListAll = filesListAll ::: filesList
        zip(exportFolder + '/' + dateString +
          "/swisscollections_eadexport_" + filenamesProInstitution(institution) +
          "_" + dateString + ".zip", filesList)
      }

    }
    //zip archive for all institutions together
    zip(exportFolder + '/' + dateString + "/swisscollections_eadexport_all_" + dateString + ".zip", filesListAll)

    val (numberOfInvalidFiles, output) = getNumberOfInvalidFiles(filesListAll, eadXsdLocation)

    //write validation result to file
    logger.info("Write validation results to validation_results.txt")
    val pw = new PrintWriter(new File(exportFolder + '/' + dateString + '/' + "validation_results.txt"))
    pw.write(output)
    pw.close()

    val totalNumberOfFiles = filesListAll.length

    //delete temporary files
    val dir = new Directory(new File(currentTempFolder))
    dir.deleteRecursively()

    sendNotificationMail(numberOfInvalidFiles, totalNumberOfFiles)

    logger.info(s"$numberOfInvalidFiles file(s) out of $totalNumberOfFiles were unvalid.")
  }

  /**
   * This checks that the files in filesList are valid with respect to the schema
   * supplied via environment variable. On top of that write the validation results
   * to a file.
   *
   * @param filesList the files to check (a list of strings containing the location of the files to validate)
   * @param xsdSchema the xsd schema against which to validate
   * @return a Tuple with (the number of invalid files in the list, the output of the validation as a string)
   */
  def getNumberOfInvalidFiles(filesList:List[String], xsdSchema: String): (Int, String) = {
    //check xml schema validity
    val totalNumberOfFiles = filesList.length
    logger.info("Xml validation of " + totalNumberOfFiles + " files")

    val stdout = new StringBuilder
    val stderr = new StringBuilder

    var numberOfInvalidFiles = 0
    for (file <- filesList) {
      try {
        Process(s"xmllint --noout --schema $xsdSchema $file") !! ProcessLogger(stdout append _, stderr append _)
        stderr.append("\n")
      } catch {
        case e: RuntimeException => numberOfInvalidFiles = numberOfInvalidFiles + 1
      }
    }
    //xmllint logs to stderr only
    (numberOfInvalidFiles, stderr.toString())
  }



  /**
   * This exports the archives (Nachlässe) of a given institution
   * @param institution the institution code (used to define the folder where to store the files)
   * @param searchQuery the solr search query for the top node of every archive (Bestand level)
   * @param currentTempFolder the directory where to store the temporary files (before zipping them together)
   * @return the list of id's of the top nodes of every archive, this is needed afterwards to process the pseudo-archives
   */
  def exportArchivesFromInstitution(institution: String, searchQuery: String, currentTempFolder: String): List[String] = {
    logger.info("Processing " + institution)
    val topIdList = client.getAllArchivesIdentifiers(searchQuery)
    logger.info("Processing " + topIdList.length + " archives")

    val dir = new Directory(new File(currentTempFolder + "/" + institution))
    dir.createDirectory()
    val exclusionCriteria = exclusionCriterionsForArchives(institution)


    for (id <- topIdList) {
      val topRecord = client.getById(id)
      //logger.info("Processing Archive " + topRecord.get("id"))
      val finalResult = buildTree(topRecord,exclusionCriteria)
      val p = new PrettyPrinter(80, 4, minimizeEmpty = true)
      //logger.info(p.format(scala.xml.XML.loadString(finalResult)))
      val pw = new PrintWriter(new File(currentTempFolder + "/" + institution + "/" + prefixFilename + topRecord.get("id") + ".xml"))
      pw.write(p.format(scala.xml.XML.loadString(finalResult)))
      pw.close

    }
    topIdList
  }

  /**
   * For all letters, manuscripts, ... which are not part of a given archive (via the parent record linking via 830 marc fields),
   * we create a pseudo archive which encompasses all these documents
   * @param institution the library code of the institution
   * @param topIdList the list of top node id's of all archives for this institution. All records which are part of an archive with this top node will be ignored
   * @param currentTempFolder the directory where to store the temporary files (before zipping them together)
   */
  def exportPseudoArchiveFromInstitution(institution: String, topIdList: List[String], currentTempFolder: String): Unit = {
    logger.info("Pseudo-archives " + institution)
    //Pseudo-Archive
    val pseudoArchivesIdentifiers = exportType match {
      case "kalliope" => client.getAllPseudoArchivesIdentifiers(searchCriterionsForPseudoArchivesKalliope(institution), topIdList)
      case "ape" => client.getAllPseudoArchivesIdentifiers(searchCriterionsForPseudoArchivesApe(institution), topIdList)
      case _ => throw new RuntimeException("invalid export type, only kalliope or ape are valid values")
    }
    if (pseudoArchivesIdentifiers.nonEmpty) {

      buildAndWritePseudoArchive(pseudoArchivesIdentifiers, institution, currentTempFolder)
    }
  }

  /**
   * Send a notification email. The mail parameters are defined in the environment variables (see readme)
   * @param numberOfInvalidFiles The number of files which didn't validate with respect to the xml schema
   * @param totalNumberOfFiles The total number of ead files generated
   */
  def sendNotificationMail(numberOfInvalidFiles: Int, totalNumberOfFiles: Int): Unit = {
    val mailer = Mailer(mailServer, mailServerPort)
      .auth(true)
      .as(emailAccount, emailPassword)
      .startTls(true)()
    val res = mailer(Envelope.from(mailFrom.split("@")(0) `@` mailFrom.split("@")(1))
      .to(mailTo.split("@")(0) `@` mailTo.split("@")(1))
      .cc(mailCc.split("@")(0) `@` mailCc.split("@")(1))
      .subject("New EAD Export")
      .content(Text(s"Hello ! \n\nA new EAD export is available. Find it " +
        s"on $downloadUrl. $numberOfInvalidFiles file(s) out of $totalNumberOfFiles were " +
        s"unvalid. See validation_results.txt file for details.")))

    Try(Await.result(res, 120.second)) match {
      case Success(res) => logger.info("Email message delivered")// we can deal with the result directly
      case Failure(e) => logger.info("Impossible to deliver email. " + e.getMessage)// but we might have to figure out if a timeout happened
    }

  }

  /**
   * Recursively build the full ead tree describing an archive
   * @param record the current record to process
   * @param level the level we are in the ead tree (0 is the top node, 1 the children of the top node, ...)
   * @return an xml string describing the tree starting at the level of the current record and going down
   */
  def buildTree(record: VuFindSolrRecord, exclusionCriteria: String, level: Int=0): String = {
    var result = ""

    val mapper = exportType match {
      case "kalliope" => new MapperKalliope()
      case "ape" => new MapperApe()
      case _ => throw new RuntimeException("invalid export type, only kalliope or ape are valid values")
    }

    if(level == 0) {
      result = exportType match {
        case "kalliope" => "<ead xmlns=\"urn:isbn:1-931666-22-9\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:isbn:1-931666-22-9 http://www.loc.gov/ead/ead.xsd\">"
        case "ape" => "<ead audience=\"external\" xmlns=\"urn:isbn:1-931666-22-9\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:isbn:1-931666-22-9 ead.xsd http://www.archivesportaleurope.net/Portal/profiles/apeEAD.xsd http://www.w3.org/1999/xlink http://www.loc.gov/standards/xlink/xlink.xsd\">"
        case _ => throw new RuntimeException("invalid export type, only kalliope or ape are valid values")
      }
      result = result + mapper.eadHeader(record).toString

      val archdesc = exportType match {
        case "kalliope" => "<archdesc level=\"collection\" id=\"CH-002121-2-" + record.get("id") + "\">"
        case "ape" => "<archdesc level=\"fonds\">"
      }
      result = result + archdesc
    }

    //try to map the record, if unsuccessful, ignore it
    val recordElement =
      try {
        mapper.mapIndividualRecord(record).toString()
      } catch {
        case e: Throwable =>
          ""
      }

    result = result + removeRecordTag(recordElement)

    val children = client.getChildren(record.get("id").toString, exclusionCriteria)

    if(level == 0 && children.size > 0) {
      result = result + "<dsc>"
    }

    result = result + processChildren(children, level, exclusionCriteria)

    if(level == 0 && children.size > 0) {
      result = result + "</dsc>"
    }

    if(level == 0) {
      result = result + "</archdesc></ead>"
    }
    result
  }

  /**
   * Process all records in the children record list and build for all of them recursively the ead tree
   * @param children list of documents to process
   * @param level the level we are in the ead tree (0 is the top node of the hierarchy)
   * @return an xml string describing all the documents in the children list and their descendants
   */
  def processChildren(children: List[SolrDocument], level: Int, exclusionCriteria: String): String = {
    var result = ""
    for (child <- children) {
      val childVufindRecord = new VuFindSolrRecord(child)
      var firstDescriptionLevelName = child.getFirstValue("description_level_name_str_mv")
      if (firstDescriptionLevelName == null || !eadLevel.contains(firstDescriptionLevelName.toString)) {
        firstDescriptionLevelName = "dossier"
      }

      result = result + "<c level=\"" + eadLevel(firstDescriptionLevelName.toString) + "\" id=\"CH-002121-2-" + child.get("id") + "\">"
      result = result + buildTree(childVufindRecord, exclusionCriteria, level + 1)
      result = result + "</c>"
    }
    result
  }

  /**
   * The pseudo archive ead file has only one level. A top node, and a list of children. This builds this record.
   * In this case we write directly the ead file incrementally on disk as a pseudo-archive can be very big, therefore
   * it is more efficient to do it like this
   * @param identifierList the identifiers of all records we need to attach to the pseudo archive
   * @param institution the institution library code
   * @param currentTempFolder the directory where to store the temporary files (before zipping them together)
   *
   * @return an xml string describing the pseudo-archive
   */
  def buildAndWritePseudoArchive(identifierList:List[String], institution:String, currentTempFolder:String): Unit = {

    val p = new PrettyPrinter(80, 4, minimizeEmpty = true)
    val pw = new PrintWriter(new File(currentTempFolder + "/" + institution + "/" +
      prefixFilename + almaIdentifierForPseudoArchives(institution) + suffixPseudoArchives + ".xml"))

    val mapper = exportType match {
      case "kalliope" => new MapperKalliope()
      case "ape" => new MapperApe()
      case _ => throw new RuntimeException("invalid export type, only kalliope or ape are valid values")
    }

    var header = exportType match {
      case "kalliope" => "<ead xmlns=\"urn:isbn:1-931666-22-9\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:isbn:1-931666-22-9 http://www.loc.gov/ead/ead.xsd\">"
      case "ape" => "<ead audience=\"external\" xmlns=\"urn:isbn:1-931666-22-9\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:isbn:1-931666-22-9 ead.xsd http://www.archivesportaleurope.net/Portal/profiles/apeEAD.xsd http://www.w3.org/1999/xlink http://www.loc.gov/standards/xlink/xlink.xsd\">"
      case _ => throw new RuntimeException ("invalid export type, only kalliope or ape are valid values")
    }
    header = header + mapper.pseudoArchiveHeader(institution).toString

    val archdesc = exportType match {
      case "kalliope" => "<archdesc level=\"collection\" id=\"CH-002121-2-" + almaIdentifierForPseudoArchives(institution) + "\">"
      case "ape" => "<archdesc level=\"fonds\">"
      case _ => throw new RuntimeException("invalid export type, only kalliope or ape are valid values")
    }
    header = header + archdesc
    header = header + mapper.pseudoArchiveDummyRecordDid(institution).toString
    header = exportType match {
      case "kalliope" => header + mapper.pseudoArchiveDummyRecordOdd.toString
      case "ape" => header
      case _ => throw new RuntimeException("invalid export type, only kalliope or ape are valid values")
    }
    header = header + "<dsc>"

    pw.write(header)

    //todo print header

    for (identifier <- identifierList) {
      val record = client.getById(identifier)
      var result = ""
      result = result + "<c level=\"file\" id=\"CH-002121-2-" + record.get("id") + "\">"
      val recordElement =
        try {
          mapper.mapIndividualRecord(record).toString()
          //try to map the record, if unsuccessful, ignore it
        } catch {
          case e: Throwable =>
            logger.warn("Problem with record " + identifier)
            logger.warn(e.getMessage)
            logger.warn(e.getStackTrace)
            ""
        }
      result = result + removeRecordTag(recordElement)
      result = result + "</c>"
      pw.write(p.format(scala.xml.XML.loadString(result)))
    }

    val footer = "</dsc></archdesc></ead>"
    pw.write(footer) //to print footer
    pw.close
  }

  // map with concordance indexed 351$c and ead level attributes
  private lazy val eadLevel = Map(
    "bestand" -> "collection",
    "teilbestand" -> "fonds",
    "serie" -> "class",
    "teilserie" -> "class",
    "dossier" -> "file",
    "teildossier" -> "file",
    "dokument" -> "item",
    "abteilung" -> "file",
    "hauptabteilung" -> "file"
  )

  /**
   * To be able to test the whole process correctly, a <record> tag encloses all mapped record
   * However this must be removed in the ead export, which is the purpose of this method
   * @param recordElement the record where the <record> tag must be removed
   * @return the record without the <record> tag. If recordElement doesn't start and end with a record tag
   *         then return an empty string
   */
  def removeRecordTag(recordElement: String) : String = {
    //We use the mapped record, but without the starting <record> tag and the end </record> tag
    val tagToRemove = "<record>"
    val numberOfCharacters = tagToRemove.length
    if (recordElement.startsWith("<record>") && recordElement.endsWith("</record>")) {
      //We use the mapped record, but without the starting <record> tag and the end </record> tag
      recordElement.substring(numberOfCharacters, recordElement.length - 2 * numberOfCharacters)
    } else {
      logger.warn("There was a problem when mapping " + recordElement)
      ""
    }
  }

  /**
   * Get the solr client
   * @return the solr client
   */
  def getSolrClient: SolrJClientWrapper = {
    val solrWrapper = SolrJClientWrapper(
      solrCollection,
      connectionTimeout,
      socketTimeout,
      nodeURL
    )
    solrWrapper
  }

  /**
   * Zip files in a folder and remove the folder structures. The resulting zip file doesn't
   * have the folder structure (with institution) of the provided files. Zipped filenames must start with ead
   * to ensure this
   * @param outputFile the path of the output file where to zip all the files
   * @param files an iterable containing the file paths of all the files to zip
   */
  def zip(outputFile: String, files: Iterable[String]): Unit = {
    val zip = new ZipOutputStream(new FileOutputStream(outputFile))
    files.foreach { name =>
      //remove the folder structure and have a flat structure in the zip file
      val path: Path = Paths.get(name)
      val fileName: String = path.getFileName.toString
      zip.putNextEntry(new ZipEntry(fileName))
      val in = new BufferedInputStream(new FileInputStream(name))
      var b = in.read()
      while (b > -1) {
        zip.write(b)
        b = in.read()
      }
      in.close()
      zip.closeEntry()
    }
    zip.close()
  }

  /**
   * Get the list of files in a directory
   * @param dir the directory to search
   * @return the list of files
   */
  def getListOfFiles(dir: String):List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }
}
