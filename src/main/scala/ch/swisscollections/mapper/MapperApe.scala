package ch.swisscollections.mapper

import ch.swisscollections.App.almaIdentifierForPseudoArchives
import org.apache.logging.log4j.scala.Logging

import java.text.SimpleDateFormat
import java.util.Calendar
import scala.language.postfixOps
import scala.xml.NodeSeq.{Empty, seqToNodeSeq}
import scala.xml._
import scala.xml.transform.{RewriteRule, RuleTransformer}

class MapperApe extends MapperKalliope with Logging{

  override def eadHeader(record: VuFindSolrRecord): Elem = {
    <eadheader langencoding="iso639-2b" scriptencoding="iso15924" dateencoding="iso8601" countryencoding="iso3166-1" repositoryencoding="iso15511" relatedencoding="Marc21">
      <eadid countrycode="CH" identifier={getISIL(record) + "_" + record.get("id")} mainagencycode={getISIL(record)}>{record.get("id")}</eadid>
      <filedesc>
        <titlestmt>
          <titleproper>{getUnitTitle(record)}</titleproper>
        </titlestmt>
      </filedesc>
      <profiledesc>
        <langusage>
          {if (getLibraryName(record) == "Bern UB Medizingeschichte: Rorschach-Archiv") {
          <language scriptcode="Latn" langcode="eng">Englisch</language>
        }
        else {<language scriptcode="Latn" langcode="ger">Deutsch</language>}
          }
        </langusage>
      </profiledesc>
    </eadheader>
  }

  override def pseudoArchiveHeader(institution: String): Elem = {
    <eadheader langencoding="iso639-2b" scriptencoding="iso15924" dateencoding="iso8601" countryencoding="iso3166-1" repositoryencoding="iso15511" relatedencoding
    ="Marc21">
      <eadid countrycode="CH" identifier={isil(institution) + "_" + almaIdentifierForPseudoArchives(institution)} mainagencycode={isil(institution)}>{almaIdentifierForPseudoArchives(institution)}</eadid>
      <filedesc>
        <titlestmt>
          <titleproper>Einzeldokumente {libraryNameGND(institution)}</titleproper>
        </titlestmt>
      </filedesc>
      <profiledesc>
        <langusage>
          <language scriptcode="Latn" langcode="ger">Deutsch</language>
        </langusage>
      </profiledesc>
    </eadheader>
  }

  override def pseudoArchiveDummyRecordDid(institution: String): Elem = {
    <did>
      <repository>{libraryName(institution)}</repository>
      <origination>
        <corpname authfilenumber={libraryGND(institution)}>
          {libraryNameGND(institution) + " (Bestandsbildner)"}
        </corpname>
      </origination>
      <unittitle>Einzeldokumente {libraryNameGND(institution)}</unittitle>
    </did>
  }

  override def mapIndividualRecord(record: VuFindSolrRecord): Elem = {
    val result = {
        <record>
          {getDid(record)}
          {getScopeContent(record)}
          {getScopeContentAbstract(record)}
          {getArrangement(record)}
          {getUserestrict(record)}
          {getBibliographyReference(record)}
          {getBibliographyPublication(record)}
          {getAcqinfo(record)}
          {getRelatedMaterial(record)}
          {getBiogHist(record)}
          {getOtherFindaid(record)}
          {getCustodHist(record)}
          {getFindaidLinks(record)}
          {getPersonControlAccess(record)}
          {getCorporateControlAccess(record)}
          {getSubjectControlAccess(record)}
          {getPlaceControlAccess(record)}
          {getGenreControlAccess(record)}
        </record>
    }
    result
  }

  override def getDid(record: VuFindSolrRecord): NodeSeq = {

    val result = {
      <did>
        {simpleTagWithAttributes(record, "852j", "unitid", "type", "call number")}
        {simpleTagWithAttributes(record, "949h", "unitid", "type", "alternative call number")}
        {getFormerCallNumber(record)}
        <unitid>
          <extptr
          xlink:type="simple"
          xlink:show="embed"
          xlink:actuate="onLoad"
          xlink:href={generateLink(record)}
          xlink:title="Katalogeintrag in swisscollections"/>
        </unitid>
        {getObjectLinks(record)}
        {getEmanuscriptaThumbnail(record)}
        <repository>{getLibraryName(record)}</repository>
        {getLanguage(record)}
        {getOriginationPerson(record)}
        {getOriginationCorporate(record)}
        {getOriginationConference(record)}
        <unittitle>{getUnitTitle(record)}</unittitle>
        {getUnitDate(record)}
        {simpleTag(record, "348a", "materialspec")}
        {getPhysDesc(record)}
        {getNote(record)}
      </did>

    }
    result
  }

  override def getFormerCallNumber(record: VuFindSolrRecord): NodeSeq = {
    val localFields = record.getFieldsAsMap("690")

    val unitids = new NodeBuffer

    for (field <- localFields) yield {
      if (field.contains("2") && field.contains("e") && field("2") == "han-A5") {
        unitids +=
          <unitid type="former call number">{field("e")}</unitid>
      }
      else {NodeSeq.Empty}
    }


    unitids

  }

  def getObjectLinks(record: VuFindSolrRecord): NodeSeq = {
    val links = record.getFieldsAsMapInd("856", "1")

    for (link <- links) yield {
      <dao xlink:href={link.getOrElse("u", "")} xlink:title={if (link.getOrElse("z", "").contains("Digitalisat")) "Digitalisat" else link.getOrElse("z", "")}/>
    }
  }

  def getEmanuscriptaThumbnail(record: VuFindSolrRecord): NodeSeq = {
    val links = record.getFieldsAsMapInd("856", "1")

    val thumbnails = new NodeBuffer

    for (link <- links) yield {
      if (link.getOrElse("u", "").contains("http://dx.doi.org/10.7891/e-manuscripta")) {
        thumbnails +=
        <dao xlink:href={link.getOrElse("u", "").replace("http://dx.doi.org", "https://www.e-manuscripta.ch/titlepage/doi")} xlink:title="thumbnail"/>
      }
    }

    if (thumbnails.nonEmpty) thumbnails
    else NodeSeq.Empty
  }

  def getFindaidLinks(record: VuFindSolrRecord): NodeSeq = {
    val links = record.getFieldsAsMapInd("856", "2")

    val findaids = new NodeBuffer

    for (link <- links) yield {
      findaids +=
      <p><extref xlink:href={link.getOrElse("u", "")} xlink:title={link.getOrElse("z", "")}/></p>
    }

    if (findaids.nonEmpty)
      <otherfindaid>{findaids}</otherfindaid>
    else NodeSeq.Empty
  }

  override def getLanguage(record: VuFindSolrRecord): NodeSeq = {

    val languageCodes = (Set(record.getControlfield("008").substring(35, 38).replace("   ", "und").replace("zxx", "und").replace("gsw", "ger")) ++
      record.getSubfields("041", "a").getOrElse(Set()))
      .map(code => (new Elem(null, "language", scala.xml.Null, scala.xml.TopScope, false, Text(language.getOrElse(code.replace("gsw", "ger").replace("zxx", "und"), ""))), code.replace("gsw", "ger").replace("zxx", "und")))
      .map(x => x._1 % Attribute(null, "langcode", x._2, Null))

    <langmaterial>{languageCodes}</langmaterial>

  }

  override def getPhysDesc(record: VuFindSolrRecord): NodeSeq = {

    val extent = {simpleTagConcat(record, "300a", "extent", ", ")}
    val dimensions = {simpleTag(record, "300c", "dimensions")}

    if (extent.nonEmpty || dimensions.nonEmpty) {
      <physdesc>
        {extent}
        {dimensions}
      </physdesc>
    }
    else {NodeSeq.Empty}
  }

  override def getNote(record: VuFindSolrRecord): NodeSeq = {
    val notes = record.getFieldsAsMap("500")

    val tag = new NodeBuffer

    {for (note <- notes) yield {
      if (note.contains("a")) {
        tag +=
        <note label="Bemerkung">
          <p>{note.getOrElse("a", "")}</p>
        </note>
      }
      else {NodeSeq.Empty}
    }
    }
    if (tag.nonEmpty) {
      tag
    }
    else {NodeSeq.Empty}
  }

  /**
   * @param record The VuFind Solr Record
   * @return tag origination generated from 100/700 where $4 = cre (Aktenbildner)
   */
  override def getOriginationPerson(record: VuFindSolrRecord): NodeSeq = {
    val persons = record.getFieldsAsMap("100") ++ record.getFieldsAsMap("700")

    {origination(persons, "pers")}
  }

  /**
   * @param record The VuFind Solr Record
   * @return tag origination generated from 110/710 where $4 = cre (Aktenbildner)
   */
  override def getOriginationCorporate(record: VuFindSolrRecord): NodeSeq = {
    val corporates = record.getFieldsAsMap("110") ++ record.getFieldsAsMap("710")

    {origination(corporates, "corp")}
  }

  /**
   * @param record The VuFind Solr Record
   * @return tag origination generated from 111/711 where $4 = cre (Aktenbildner)
   */
  override def getOriginationConference(record: VuFindSolrRecord): NodeSeq = {
    val conferences = record.getFieldsAsMap("111") ++ record.getFieldsAsMap("711")

    {origination(conferences, "conf")}
  }

  override def origination(creators: Seq[Map[String, String]], persOrCorpOrConf: String): NodeSeq = {
    val origination = new NodeBuffer

    for (creator <- creators) yield {
      if (creator.getOrElse("4", "") == "cre") {
        origination +=
          <origination>
            {
            if (persOrCorpOrConf == "pers") {
              {persname(creator, "persname")}
            }
            else if (persOrCorpOrConf == "corp") {
              {corpname(creator, "b")}
            }
            else if (persOrCorpOrConf == "conf") {
              {corpname(creator,"e")}
            }
            }
          </origination>
      }
    }

    if (origination.nonEmpty) {
      origination
    }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The VuFind Solr Record
   * @return tag controlacccess for persons from x00
   */
  override def getPersonControlAccess(record: VuFindSolrRecord): NodeSeq = {
    val persons = record.getFieldsAsMap("100") ++ record.getFieldsAsMap("700")
    val subPersons = record.getFieldsAsMap("600")

    val controlAccess = new NodeBuffer

    if (persons.nonEmpty || subPersons.nonEmpty) {
          {for (person <- persons) yield {
          if (person.getOrElse("4", "") != "cre") {
            controlAccess +=
            {persname(person,"persname")}
          }
          else {NodeSeq.Empty}
        }
          }
          {for (subPerson <- subPersons) yield {
          if (subPerson.getOrElse("c", "").contains("Familie")) {
            controlAccess +=
            persname(subPerson, "famname")
          }
          else {
            controlAccess +=
            persname(subPerson, "persname")
          }
        }
          }
    }

    if (controlAccess.nonEmpty) {
      <controlaccess><head>Personen</head>{controlAccess}</controlaccess>
    }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The VuFind Solr Record
   * @return tag controlaccess for corporates from fields x10, x11
   */
  override def getCorporateControlAccess(record: VuFindSolrRecord): NodeSeq = {
    val corporates = record.getFieldsAsMap("110") ++ record.getFieldsAsMap("710") ++ record.getFieldsAsMap("610")
    val conferences = record.getFieldsAsMap("111") ++ record.getFieldsAsMap("711") ++ record.getFieldsAsMap("611")

    val controlAccess = new NodeBuffer

    if (corporates.nonEmpty || conferences.nonEmpty) {
          {for (corporate <- corporates) yield {
          if (corporate.getOrElse("4", "") != "cre") {
            controlAccess +=
            {corpname(corporate, "b")}
          }
          else {NodeSeq.Empty}
        }
          }
          {for (conference <- conferences) yield {
          if (conference.getOrElse("4", "") != "cre") {
            controlAccess +=
            {corpname(conference, "e")}
          }
          else {NodeSeq.Empty}
        }
          }
    }

    if (controlAccess.nonEmpty) {
      <controlaccess><head>Körperschaften</head>{controlAccess}</controlaccess>
    }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The VuFind Solr Record
   * @return tag controlacces from field 650
   */
  override def getSubjectControlAccess(record: VuFindSolrRecord): NodeSeq = {
    val subjects = record.getFieldsAsMap("650")

    if (subjects.nonEmpty) {
        <controlaccess>
          <head>Sachschlagwörter</head>
          {for (subject <- subjects) yield {
          subjectTag(subject)
        }
          }
        </controlaccess>
    }
    else {NodeSeq.Empty}
  }

  /**
   * @param value a field 6xx
   * @return subject tag, concat $a -- $v -- $x -- $y -- $z
   */
  override def subjectTag(value: Map[String, String]): Node = {
    val content: String =
      {
        (if (value.contains("a")) value.getOrElse("a", "") else "") +
          (if (value.contains("v")) " -- " + value.getOrElse("v", "") else "") +
          (if (value.contains("x")) " -- " + value.getOrElse("x", "") else "") +
          (if (value.contains("y")) " -- " + value.getOrElse("y", "") else "") +
          (if (value.contains("z")) " -- " + value.getOrElse("z", "") else "")
      }

    val gndId = {
      if (value.getOrElse("0", "").startsWith("(DE-588)")) {
        value.getOrElse("0", "").substring(8)
      }
      else ""
    }

    val tag = new Elem(null, "subject", scala.xml.Null, scala.xml.TopScope, false, Text(content))

    if (gndId.nonEmpty) {
      tag % Attribute(null, "authfilenumber", gndId, Null)
    }
    else {
      tag
    }

  }

  /**
   * @param record The VuFind Solr Record
   * @return controlaccess for places from fields 651, 751
   */
  override def getPlaceControlAccess(record: VuFindSolrRecord): NodeSeq = {
    val places = record.getFieldsAsMap("751")
    val subPlaces = record.getFieldsAsMap("651")

    if (places.nonEmpty || subPlaces.nonEmpty) {
        <controlaccess>
          <head>Orte</head>
          {for (place <- places) yield {
          geogname(place)
        }
          }
          {for (subPlace <- subPlaces) yield {
          geogname(subPlace)
        }
          }
        </controlaccess>
    }
    else {NodeSeq.Empty}

  }

  def geogname(value: Map[String, String]): Node = {
    val content: String =
    {
      (if (value.contains("a")) value.getOrElse("a", "") else "") +
        (if (value.contains("v")) " -- " + value.getOrElse("v", "") else "") +
        (if (value.contains("x")) " -- " + value.getOrElse("x", "") else "") +
        (if (value.contains("y")) " -- " + value.getOrElse("y", "") else "") +
        (if (value.contains("z")) " -- " + value.getOrElse("z", "") else "")
    }

    val gndId = {
      if (value.getOrElse("0", "").startsWith("(DE-588)")) {
        value.getOrElse("0", "").substring(8)
      }
      else ""
    }

    val tag = new Elem(null, "geogname", scala.xml.Null, scala.xml.TopScope, false, Text(content))

    if (gndId.nonEmpty) {
      tag % Attribute(null, "authfilenumber", gndId, Null)
    }
    else {
      tag
    }
  }

  /**
   * @param person a person from field x00
   * @return tag persname, generated from x00, concat subfields according to $a ($q) $b, $c ($d) -- $t. $n. $p. $l. $m. $r. $s. $o. $h
   */
  override def persname(person: Map[String, String], tagName: String): Node = {

    val role = relator.getOrElse(person.getOrElse("4", ""), "")

    val content: String =
      {
        (if (person.contains("a")) person.getOrElse("a", "") else "") +
          (if (person.contains("q")) " (" + person.getOrElse("q", "") + ")" else "") +
          (if (person.contains("b")) " " + person.getOrElse("b", "") else "") +
          (if (person.contains("c")) ", " + person.getOrElse("c", "") else "") +
          (if (person.contains("d")) " (" + person.getOrElse("d", "") + ")" else "") +
          (if (person.contains("t")) " -- " + person.getOrElse("t", "") else "") +
          (if (person.contains("n")) ". " + person.getOrElse("n", "") else "") +
          (if (person.contains("p")) ". " + person.getOrElse("p", "") else "") +
          (if (person.contains("l")) ". " + person.getOrElse("l", "") else "") +
          (if (person.contains("m")) ". " + person.getOrElse("m", "") else "") +
          (if (person.contains("r")) ". " + person.getOrElse("r", "") else "") +
          (if (person.contains("s")) ". " + person.getOrElse("s", "") else "") +
          (if (person.contains("o")) ". " + person.getOrElse("o", "") else "") +
          (if (person.contains("h")) ". " + person.getOrElse("h", "") else "") +
          (if (role.nonEmpty) " (" + role + ")" else "")
      }

    val gndId = {
      if (person.getOrElse("0", "").startsWith("(DE-588)")) {
        person.getOrElse("0", "").substring(8)
      }
      else ""
    }

    val tag = new Elem(null, tagName, scala.xml.Null, scala.xml.TopScope, false, Text(content))

    if (gndId.nonEmpty) {
      tag % Attribute(null, "authfilenumber", gndId, Null)
    }
    else {
      tag
    }
  }

  /**
   * @param corporate a corporate from field x10 or x11
   * @return tag corpname for a corporate body from x10 or x11, concat subfields according to $a. $b ($g) for x10, $a. $e ($g) for x11
   */
  def corpname(corporate: Map[String, String], subfieldSubUnit: String): Node = {

    val role = relator.getOrElse(corporate.getOrElse("4", ""), "")
    val content: String =
    {
      (if (corporate.contains("a")) corporate.getOrElse("a", "") else "") +
        (if (corporate.contains(subfieldSubUnit)) ". " + corporate.getOrElse(subfieldSubUnit, "") else "") +
        (if (corporate.contains("g")) " (" + corporate.getOrElse("g", "") + ")" else "") +
        (if (role.nonEmpty) " (" + role + ")" else "")
    }

    val gndId = {
      if (corporate.getOrElse("0", "").startsWith("(DE-588)")) {
        corporate.getOrElse("0", "").substring(8)
      }
      else ""
    }


    val tag = new Elem(null, "corpname", scala.xml.Null, scala.xml.TopScope, false, Text(content))

    if (gndId.nonEmpty) {
      tag % Attribute(null, "authfilenumber", gndId, Null)
    }
    else {
      tag
    }
  }
}
