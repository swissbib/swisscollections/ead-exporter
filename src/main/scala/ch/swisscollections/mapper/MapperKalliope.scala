package ch.swisscollections.mapper

import org.apache.logging.log4j.scala.Logging

import java.text.SimpleDateFormat
import java.util.Calendar
import scala.language.postfixOps
import scala.xml.NodeSeq.seqToNodeSeq
import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml._

class MapperKalliope extends Logging{

  // Hash with concordance 852$a and ISIL
  val isil = Map(
    "B583RO"  -> "CH-000956-2",
    "SGARK"   -> "CH-000095-1",
    "SGKBV"   -> "CH-000009-3",
    "A125"    -> "CH-000133-4",
    "A125Pers"    -> "CH-000133-4",
    "A125Corp"    -> "CH-000133-4",
    "AKB"     -> "CH-000050-X",
    "A381"    -> "CH-000086-2",
    "SGSTI"   -> "CH-000093-7",
    "A100"    -> "CH-000004-7",
    "A150"    -> "CH-000045-X",
    "LUZHB"   -> "CH-000006-1",
    "B415"    -> "CH-000284-9",
    "B400"    -> "CH-000011-1",
    "B404"    -> "CH-000011-1",
    "A382"    -> "CH-000048-1",
    "Z01"     -> "CH-000008-6"
  )

  // concordance 852$b and Library Name
  val libraryName = Map(
    "B583RO"  -> "Bern UB Medizingeschichte: Rorschach-Archiv",
    "SGARK"   -> "KB Appenzell Ausserrhoden",
    "SGKBV"   -> "St. Gallen KB Vadiana",
    "A125"    -> "Basel UB Wirtschaft - SWA",
    "A125Pers"    -> "Basel UB Wirtschaft - SWA",
    "A125Corp"    -> "Basel UB Wirtschaft - SWA",
    "AKB"     -> "KB Aargau",
    "A381"    -> "KB Thurgau",
    "SGSTI"   -> "St. Gallen Stiftsbibliothek",
    "A100"    -> "Basel UB",
    "A150"    -> "Solothurn ZB",
    "LUZHB"   -> "Luzern ZHB",
    "B415"    -> "Bern UB Schweizerische Osteuropabibliothek",
    "B400"    -> "Bern UB Bibliothek Münstergasse",
    "B404"    -> "Bern UB Bibliothek Münstergasse",
    "A382"    -> "Zofingen SB",
    "Z01"     -> "ZB Zürich"
  )

  // library names to be used for the Title of the Bestand Einzeldokumente and orgination in EAD-Exports
  val libraryNameGND = Map(
    "B583RO"  -> "Archiv und Sammlung Hermann Rorschach",
    "SGARK"   -> "Kantonsbibliothek Appenzell Ausserrhoden",
    "SGKBV"   -> "Kantonsbibliothek Vadiana St. Gallen",
    "A125"    -> "Schweizerisches Wirtschaftsarchiv",
    "A125Pers"    -> "Schweizerisches Wirtschaftsarchiv",
    "A125Corp"    -> "Schweizerisches Wirtschaftsarchiv",
    "AKB"     -> "Aargauer Kantonsbibliothek",
    "A381"    -> "Kantonsbibliothek Thurgau",
    "SGSTI"   -> "Stiftsbibliothek St. Gallen",
    "A100"    -> "Universitätsbibliothek Basel",
    "A150"    -> "Zentralbibliothek Solothurn",
    "LUZHB"   -> "Zentral- und Hochschulbibliothek Luzern",
    "B415"    -> "Schweizerische Osteuropabibliothek Bern",
    "B400"    -> "Universitätsbibliothek Bern, Bibliothek Münstergasse",
    "B404"    -> "Universitätsbibliothek Bern, Bibliothek Münstergasse",
    "A382"    -> "Stadtbibliothek Zofingen",
    "Z01"     -> "Zentralbibliothek Zürich"
  )

  val libraryGND = Map (
    "B583RO"  -> "1121113443",
    "SGARK"   -> "5337772-2",
    "SGKBV"   -> "2111506-0",
    "A125"    -> "2021763-8",
    "A125Pers"    -> "2021763-8",
    "A125Corp"    -> "2021763-8",
    "AKB"     -> "4100444-9",
    "A381"    -> "10167819-8",
    "SGSTI"   -> "2089619-0",
    "A100"    -> "2023655-4",
    "A150"    -> "38429-X",
    "LUZHB"   -> "4647881-4",
    "B415"    -> "5165270-5",
    "B400"    -> "1101966483",
    "B404"    -> "1101966483",
    "A382"    -> "84370-2",
    "Z01"     -> "1012546-2"
  )

  // Hash with concordance MARC21 relator codes and ead relator codes
  val relator = Map(
    "ann" -> "Annotator",
    "arr" -> "Arrangeur",
    "art" -> "Künstler",
    "aut" -> "Verfasser",
    "bnd" -> "Buchbinder",
    "cmp" -> "Komponist",
    "cnd" -> "Dirigent",
    "col" -> "Sammler",
    "cre" -> "Bestandsbildner",
    "ctb" -> "Beiträger",
    "ctg" -> "Verfasser",
    "dsr" -> "Designer",
    "dte" -> "Widmungsschreiber",
    "dto" -> "Widmungsverfasser",
    "dub" -> "Verfasser",
    "edt" -> "Herausgeber",
    "fmk" -> "Regisseur",
    "fmo" -> "Vorbesitzer",
    "ill" -> "Illustrator",
    "lbt" -> "Librettist",
    "lyr" -> "Texter",
    "mus" -> "Musiker",
    "nrt" -> "Sprecher",
    "oth" -> "Beiträger",
    "own" -> "Inhaber",
    "pat" -> "Auftraggeber",
    "pht" -> "Fotograf",
    "ppm" -> "Papierhersteller",
    "pra" -> "Praeses",
    "prf" -> "Interpret",
    "pro" -> "Produzent",
    "prt" -> "Drucker",
    "rcp" -> "Adressat",
    "rsp" -> "Respondens",
    "scl" -> "Künstler",
    "scr" -> "Schreiber",
    "sng" -> "Künstler",
    "subF" -> "Erwähnte Familie",
    "subK" -> "Erwähnte Körperschaft",
    "subP" -> "Erwähnte Person",
    "trl" -> "Übersetzer",
  )

  // Hash with concordance MARC21 language codes and written language name
  val language = Map(
    "aar" -> "Afar",
    "afr" -> "Afrikaans",
    "alb" -> "Albanisch",
    "ang" -> "Altenglisch",
    "ara" -> "Arabisch",
    "arc" -> "Aramäisch",
    "arm" -> "Armenisch",
    "aze" -> "Azeri",
    "baq" -> "Baskisch",
    "bel" -> "Weissrussisch",
    "ben" -> "Bengali",
    "bos" -> "Bosnisch",
    "bul" -> "Bulgarisch",
    "bur" -> "Burmesisch",
    "chi" -> "Chinesisch",
    "chu" -> "Altbulgarisch, Kirchenslawisch",
    "cop" -> "Koptisch",
    "cze" -> "Tschechisch",
    "dan" -> "Dänisch",
    "dut" -> "Niederländisch",
    "egy" -> "Ägyptisch",
    "eng" -> "Englisch",
    "est" -> "Estnisch",
    "fin" -> "Finnisch",
    "fre" -> "Französisch",
    "geo" -> "Georgisch",
    "ger" -> "Deutsch",
    "gez" -> "Äthiopisch",
    "gla" -> "Gälisch",
    "gle" -> "Gälisch",
    "grc" -> "Altgriechisch",
    "gre" -> "Neugriechisch",
    "gsw" -> "Schweizerdeutsch",
    "heb" -> "Hebräisch",
    "hin" -> "Hindi",
    "hrv" -> "Kroatisch",
    "hun" -> "Ungarisch",
    "ice" -> "Isländisch",
    "ind" -> "Indonesisch",
    "ita" -> "Italienisch",
    "jav" -> "Javanisch",
    "jpn" -> "Japanisch",
    "kas" -> "Kashmiri",
    "kaz" -> "Kasachisch",
    "khm" -> "Khmer",
    "kir" -> "Kirisisch",
    "kor" -> "Koreanisch",
    "kur" -> "Kurdisch",
    "lat" -> "Lateinisch",
    "lav" -> "Lettisch",
    "lit" -> "Litauisch",
    "mac" -> "Mazedonisch",
    "may" -> "Malaiisch",
    "mon" -> "Mongolisch",
    "nor" -> "Norwegisch",
    "ota" -> "Osmanisch",
    "per" -> "Persisch",
    "pol" -> "Polnisch",
    "por" -> "Portugiesisch",
    "roh" -> "Rätoromanisch",
    "rom" -> "Romani",
    "rum" -> "Rumänisch",
    "rus" -> "Russisch",
    "san" -> "Sanskrit",
    "slo" -> "Slowakisch",
    "slv" -> "Slowenisch",
    "spa" -> "Spanisch",
    "srp" -> "Serbisch",
    "swa" -> "Swahili",
    "swe" -> "Schwedisch",
    "syr" -> "Syrisch",
    "tam" -> "Tamil",
    "tgk" -> "Tadschikisch",
    "tgl" -> "Philippinisch",
    "tha" -> "Siamesisch",
    "tuk" -> "Turkmenisch",
    "tur" -> "Türkisch",
    "ukr" -> "Ukrainisch",
    "urd" -> "Urdu",
    "uzb" -> "Usbekisch",
    "vie" -> "Vietnamisch",
    "wen" -> "Sorbisch",
    "yid" -> "Jiddisch"
  )

  // dummy libraries (A116, A117, etc.) shouldn't be exported
  // add all ZB libraries?
  val validLibrary = "A100|A125|A150|A381|A382|B400|B404|B415|B583RO|LUZHB|SGARK|SGKBV|SGSTI|AKB|Z01|Z02|Z03|Z04|Z05|Z06|Z07"

  //Inspiration : https://github.com/basimar/han_seq2ead_kalliope/blob/master/seq2ead.pl

  def eadHeader(record: VuFindSolrRecord): Elem = {
    <eadheader langencoding="iso639-2b" scriptencoding="iso15924" dateencoding="iso8601" countryencoding="iso3166-1" repositoryencoding="iso15511" relatedencoding="Marc21" audience="external">
      <eadid/>
      <filedesc>
        <titlestmt>
          <titleproper>{getUnitTitle(record)}</titleproper>
        </titlestmt>
      </filedesc>
      <profiledesc>
        <langusage>
          {if (getLibraryName(record) == "Bern UB Medizingeschichte: Rorschach-Archiv") {
          <language scriptcode="Latn" langcode="eng">Englisch</language>
        }
        else {<language scriptcode="Latn" langcode="ger">Deutsch</language>}
          }
        </langusage>
      </profiledesc>
    </eadheader>
  }

  def pseudoArchiveHeader(institution: String): Elem = {
    <eadheader langencoding="iso639-2b" scriptencoding="iso15924" dateencoding="iso8601" countryencoding="iso3166-1" repositoryencoding="iso15511" relatedencoding
    ="Marc21" audience="external">
      <eadid/>
      <filedesc>
        <titlestmt>
          <titleproper>Einzeldokumente {libraryNameGND(institution)}</titleproper>
        </titlestmt>
      </filedesc>
      <profiledesc>
        <langusage>
          <language scriptcode="Latn" langcode="ger">Deutsch</language>
        </langusage>
      </profiledesc>
    </eadheader>
  }

  def pseudoArchiveDummyRecordDid(institution: String): Elem = {
    <did>
      <repository>
        <corpname role="Bestandshaltende Einrichtung" normal={libraryName(institution)} authfilenumber={isil(institution)} source="ISIL">
          {libraryName(institution)}
        </corpname>
      </repository>
      <origination>
        <corpname normal={libraryNameGND(institution)} role="Bestandsbildner" source="GND" authfilenumber={libraryGND(institution)}>
          {libraryNameGND(institution)}
        </corpname>
      </origination>
      <unittitle>Einzeldokumente {libraryNameGND(institution)}</unittitle>
    </did>
  }

  def pseudoArchiveDummyRecordOdd: Elem = {
    <odd>
      <head>Steuerfelder</head>
      <list>
        <item>
          <date type="Erfassungsdatum" normal="20220725">25.07.2022</date>
          <date type="Synchronisationsdatum" normal=
          {val format = new SimpleDateFormat("yyyyMMdd")
          format.format(Calendar.getInstance().getTime)}>{val format = new SimpleDateFormat("dd.MM.yyyy")
          format.format(Calendar.getInstance().getTime)}</date>
        </item>
      </list>
    </odd>
  }

  /**
   * This maps a single record to the ead format
   * @param record
   * @return XML Element with <record> tag at the beginning
   */
  def mapIndividualRecord(record: VuFindSolrRecord): Elem = {
    val result = {
        <record>
          {getDid(record)}
          {getScopeContent(record)}
          {getScopeContentAbstract(record)}
          {getArrangement(record)}
          {getUserestrict(record)}
          {getBibliographyReference(record)}
          {getBibliographyPublication(record)}
          {getAcqinfo(record)}
          {getRelatedMaterial(record)}
          {getBiogHist(record)}
          {getOtherFindaid(record)}
          {getCustodHist(record)}
          {getPersonControlAccess(record)}
          {getCorporateControlAccess(record)}
          {getSubjectControlAccess(record)}
          {getPlaceControlAccess(record)}
          {getGenreControlAccess(record)}
          <odd>
            <head>Steuerfelder</head>
            <list>
              <item>
                {getCreateDate(record)}
                {getModificationDate(record)}
                <date type="Synchronisierungsdatum" normal=
                {val format = new SimpleDateFormat("yyyyMMdd")
                  format.format(Calendar.getInstance().getTime)}>{val format = new SimpleDateFormat("dd.MM.yyyy")
                  format.format(Calendar.getInstance().getTime)}</date>
              </item>
            </list>
          </odd>
        </record>
    }
    result
  }

  def getDid(record: VuFindSolrRecord): NodeSeq = {

    val result = {
      <did>
        <unitid label="Signatur">{getUnitid(record)}</unitid>
        {simpleTagWithAttributes(record, "949h", "unitid", "label", "Weitere Signatur")}
        {getFormerCallNumber(record)}
        <dao
          xlink:type="simple"
          xlink:show="embed"
          xlink:actuate="onLoad"
          xlink:href={generateLink(record)}
          xlink:title="Katalogeintrag in swisscollections">
        </dao>
        {getLinks(record)}
        <repository>
          <corpname role="Bestandshaltende Einrichtung" normal={getLibraryName(record)} authfilenumber={getISIL(record)} source="ISIL">
            {getLibraryName(record)}
          </corpname>
        </repository>
        {getLanguage(record)}
        {getOriginationPerson(record)}
        {getOriginationCorporate(record)}
        {getOriginationConference(record)}
        <unittitle>{getUnitTitle(record)}</unittitle>
        {getUnitTitleAlt(record)}
        {getUnitDate(record)}
        {simpleTag(record, "348a", "materialspec")}
        {getPhysDesc(record)}
        {getNote(record)}
        {simpleTagWithAttributes(record, "525a", "abstract", "type", "Darin")}
      </did>

    }
    result
  }

  def getUnitTitle(record: VuFindSolrRecord): String = {
    val mainTitle = {record.getFirstSubfield("245","a").getOrElse("")}
    val subTitle = {record.getFirstSubfield("245", "b").getOrElse("")}
    val responsibility = {record.getFirstSubfield("245", "c").getOrElse("")}

    if (subTitle.nonEmpty && responsibility.nonEmpty) {
      mainTitle + " : " + subTitle + " / " + responsibility
    }
    else if (subTitle.nonEmpty) {
      mainTitle + " : " + subTitle
    }
    else if (responsibility.nonEmpty) {
      mainTitle + " / " + responsibility
    }
    else mainTitle
  }

  /**
   * @param record The VuFind Solr Record
   * @return tag unittitle for alternative titles from 246, concat $a. $n. $p
   */
  def getUnitTitleAlt(record: VuFindSolrRecord): NodeSeq = {
    val titles = record.getFieldsAsMap("246")

    {for (title <- titles) yield {
      if (title.contains("i")) {
        <unittitle label={title.getOrElse("i", "")}>
          {alternateTitle(title)}
        </unittitle>
      }
      else {
        <unittitle label="Weiterer Titel">
          {alternateTitle(title)}
        </unittitle>
      }
    }
    }

  }

  def alternateTitle(title: Map[String, String]): Node = {
    <title>{
      (if (title.contains("a")) title.getOrElse("a", "") else "") +
        (if (title.contains("n")) ". " + title.getOrElse("n", "") else "") +
        (if (title.contains("p")) ". " + title.getOrElse("p", "") else "")
      }</title>
  }

  def getFormerCallNumber(record: VuFindSolrRecord): NodeSeq = {
    val localFields = record.getFieldsAsMap("690")

    val unitids = new NodeBuffer

    for (field <- localFields) yield {
      if (field.contains("2") && field.contains("e") && field("2") == "han-A5") {
        unitids +=
        <unitid label="Frühere Signatur">{field("e")}</unitid>
      }
      else {NodeSeq.Empty}
    }


    unitids

  }

  def generateLink(record: VuFindSolrRecord): String =
    "https://swisscollections.ch/Record/" + record.get("id")

  def getLibraryName(record: VuFindSolrRecord): String = {
    val holdings = record.getFieldsAsMap("852")
    libraryName.getOrElse(holdings.apply(holdings.indexWhere(_.getOrElse("b", "").matches(validLibrary))).getOrElse("b",""), "")
  }

  def getISIL(record: VuFindSolrRecord): String = {
    val holdings = record.getFieldsAsMap("852")
    isil.getOrElse(holdings.apply(holdings.indexWhere(_.getOrElse("b", "").matches(validLibrary))).getOrElse("b",""), "")
  }

  def getUnitid(record: VuFindSolrRecord): String = {
    val holdings = record.getFieldsAsMap("852")
    holdings.apply(holdings.indexWhere(_.getOrElse("b", "").matches(validLibrary))).getOrElse("j", "")
  }

  def getLinks(record: VuFindSolrRecord): NodeSeq = {
    val links = record.getFieldsAsMap("856")

    for (link <- links) yield {
      <dao xlink:href={link.getOrElse("u", "")} xlink:title={link.getOrElse("z", "")}></dao>
    }

    //links.map(link => <dao xlink:href={link.getOrElse("u", "")} xlink:title={link.getOrElse("z", "")}></dao>)

  }

  /**
   * @param record The Vufind Solr Recrod
   * @return creation date created from 008/00-05
   */
  def getCreateDate(record: VuFindSolrRecord): NodeSeq = {
    val day = {record.getControlfield("008").substring(4, 6)}
    val month = {record.getControlfield("008").substring(2, 4)}
    val year = {record.getControlfield("008").substring(0, 2).toInt}
    val dateNorm = s"${if (year >= 60) "19" else if (year <= 9) "200" else "20"}$year$month$day"
    val dateReadable = s"$day.$month.${if (year >= 60) "19" else if (year <= 9) "200" else "20"}$year"

    val tag = {
      <date type="Erfassungsdatum">{dateReadable}</date>
    }

    if (validateDate(dateNorm)) {tag % Attribute(null, "normal", dateNorm, Null)}
    else tag

  }

  /**
   * @param record The Vufind Solr Recrod
   * @return modification date from 005 in normalized and human readable form
   */
  def getModificationDate(record: VuFindSolrRecord): NodeSeq = {

      val field = "005"
      val text = record.getControlfieldOption(field)
      text match {
        case None => NodeSeq.Empty
        case Some(value) =>
          val dateFormat = new SimpleDateFormat("yyyyMMdd")
          val date = dateFormat.parse(value.substring(0,8))
          val dateNormalized = dateFormat.format(date)
          val formatReadable = new SimpleDateFormat("dd.MM.yyyy")
          val dateReadable = formatReadable.format(date)
          val tag = new Elem(null, "date", scala.xml.Null, scala.xml.TopScope, false, Text(dateReadable))
          tag % Attribute(null, "normal", dateNormalized, Attribute(null, "type", "Modifikationsdatum", Null))

      }
  }

  def getUnitDate(record: VuFindSolrRecord): NodeSeq = {
    val dateExact = record.getFieldsAsMap("046")
    val dateDesc = record.getFirstSubfield("264", "c").getOrElse("")

    if (dateExact.length == 1) {
      exactUnitDate(dateExact.head, dateDesc)
    }
    else {yearUnitDate(record, dateDesc)}
  }

  def exactUnitDate(date: Map[String, String], dateDesc: String): NodeSeq = {
    val startDateBC = cleanDate(date.getOrElse("b", ""))
    val startDate = cleanDate(date.getOrElse("c", ""))
    val endDateBC = cleanDate(date.getOrElse("d", ""))
    val endDate = cleanDate(date.getOrElse("e", ""))

    val dateNorm = {
      if (startDateBC.nonEmpty) {
        "-" + startDateBC +
          (if (endDateBC.nonEmpty) "/" + "-" + endDateBC
          else if (endDate.nonEmpty) "/" + endDate
          else "")
      }
      else if (startDate.nonEmpty) {
        startDate +
          (if (endDateBC.nonEmpty) "/" + "-" + endDateBC
          else if (endDate.nonEmpty) "/" + endDate
          else "")
      }
      else if (endDateBC.nonEmpty) {
        "-" + endDateBC
      }
      else if (endDate.nonEmpty) {
        endDate
      }
      else ""
    }

    val dateReadable = {
      if (startDateBC.nonEmpty) {
        "v" + getDate(startDateBC) +
          (if (endDateBC.nonEmpty) "-" + "v" + getDate(endDateBC)
          else if (endDate.nonEmpty) "-" + getDate(endDate)
          else "")
      }
      else if (startDate.nonEmpty) {
        getDate(startDate) +
          (if (endDateBC.nonEmpty) "-" + "v" + getDate(endDateBC)
          else if (endDate.nonEmpty) "-" + getDate(endDate)
          else "")
      }
      else if (endDateBC.nonEmpty) {
        "v" + getDate(endDateBC)
      }
      else if (endDate.nonEmpty) {
        getDate(endDate)
      }
      else ""
    }

    val tag = {
      if (dateDesc.nonEmpty) {
        new Elem(null, "unitdate", scala.xml.Null, scala.xml.TopScope, false, Text(dateDesc))
      }
      else {new Elem(null, "unitdate", scala.xml.Null, scala.xml.TopScope, false, Text(dateReadable))}
    }

    if (validateDate(dateNorm)) {tag % Attribute(null, "normal", dateNorm, Null)}
    else tag

  }

  def yearUnitDate(record: VuFindSolrRecord, dateDesc: String): NodeSeq = {
    val startYear = {record.getControlfield("008").substring(7, 11)}
    val endYear = {record.getControlfield("008").substring(11, 15)}

    val dateNorm = {
      if (startYear != "----" && startYear != "uuuu"  && startYear != "    ") {
        startYear +
          (if (endYear != "----" && endYear != "uuuu"  && endYear != "    ") "/" + endYear else "")
      }
      else if (endYear != "----" && endYear != "uuuu"  && endYear != "    ") {
        endYear
      }
      else ""
    }

    if (dateNorm.nonEmpty) {
      val tag = {
        if (dateDesc.nonEmpty) {
          new Elem(null, "unitdate", scala.xml.Null, scala.xml.TopScope, false, Text(dateDesc))
        }
        else {new Elem(null, "unitdate", scala.xml.Null, scala.xml.TopScope, false, Text(dateNorm.replace("/", "-")))}
      }
      if (validateDate(dateNorm)) {tag % Attribute(null, "normal", dateNorm, Null)}
      else tag
    }
    else {NodeSeq.Empty}
  }

  def getDate(date: String): String = {
    if (date.length == 4) {
      date
    }
    else if (date.length == 6) {
      date.substring(4,6) + "." + date.substring(0,4)
    }
    else if (date.length == 8) {
      date.substring(6,8) + "." + date.substring(4,6) + "." + date.substring(0,4)
    }
    else {
      ""
    }
  }

  def cleanDate (rawDate: String): String = {
    val date = rawDate.replace(".", "").replace("[", "").replace("]", "")
    date match {
      case date if date.length == 6 && date.substring(4,6).toInt > 12 => date.substring(0,4)
      case date if date.length == 6 => date.substring(0,4) + "-" + date.substring(4,6)
      case date if date.length == 8 && date.endsWith("0000") => date.substring(0,4)
      case date if date.length == 8 && date.endsWith("00") => date.substring(0,4) + "-" + date.substring(4,6)
      case _ => date
    }
  }

  def getLanguage(record: VuFindSolrRecord): NodeSeq = {

    val languageCodes = (Set(record.getControlfield("008").substring(35, 38).replace("   ", "und").replace("zxx", "und")) ++
      record.getSubfields("041", "a").getOrElse(Set()))
      .map(code => (new Elem(null, "language", scala.xml.Null, scala.xml.TopScope, false, Text(language.getOrElse(code, ""))), code))
      .map(x => x._1 % Attribute(null, "langcode", x._2, Null))

    <langmaterial>{languageCodes}</langmaterial>

  }

  def getPhysDesc(record: VuFindSolrRecord): NodeSeq = {

    val physfacet = {simpleTagWithAttributes(record, "250a", "physfacet", "label", "Ausreifungsgrad")}
    val extent = {simpleTagConcat(record, "300a", "extent", ", ")}
    val dimensions = {simpleTag(record, "300c", "dimensions")}
    val material = {simpleTagWithAttributesConcat(record, "340a", "physfacet", "label", "Material", ", ")}
    val binding = {simpleTagWithAttributes(record, "563a", "physfacet", "label", "Einband")}

    if (physfacet.nonEmpty || extent.nonEmpty || dimensions.nonEmpty || material.nonEmpty || binding.nonEmpty) {
      <physdesc>
        {physfacet}
        {extent}
        {dimensions}
        {material}
        {binding}
      </physdesc>
    }
    else {NodeSeq.Empty}
  }

  def getNote(record: VuFindSolrRecord): NodeSeq = {
    val notes = record.getFieldsAsMap("500")

    val tag = new NodeBuffer

    {for (note <- notes) yield {
      if (note.contains("a")) {
        tag +=
        <note label="Bemerkung" audience="external">
          <p>{note.getOrElse("a", "")}</p>
        </note>
      }
      else {NodeSeq.Empty}
    }
    }
    if (tag.nonEmpty) {
      tag
    }
    else {NodeSeq.Empty}
  }

  /**
   * @param record The Vufind Solr Recrod
   * @return tag scopecontent: get each field 596 3 (replaces 505), subfields n, g, t, r, i, s, v and concat according to pattern: $n. ($g) $t / $r. $i $s - $v ; write p tag for each field 596
   */
  def getScopeContent(record: VuFindSolrRecord): NodeSeq = {
    val contents = record.getFieldsAsMap("596", "3")

    if (contents.nonEmpty) {
      <scopecontent>
        <head>Inhaltsangabe</head>
        {for (content <- contents) yield {
        <p>{
          //content.get("n").map(n => s" ($n)").getOrElse("")
          ((if (content.contains("n")) content.getOrElse("n", "") + "." else "") +
            (if (content.contains("g")) " (" + content.getOrElse("g", "") + ")" else "") +
            (if (content.contains("t")) " " + content.getOrElse("t", "") else "") +
            (if (content.contains("r")) " / " + content.getOrElse("r", "") else "") +
            (if (content.contains("i")) ". " + content.getOrElse("i", "") else  "") +
            (if (content.contains("s")) " " + content.getOrElse("s", "") else "") +
            (if (content.contains("v")) " - " + content.getOrElse("v", "") else "")).trim
          }</p>
      }
        }
      </scopecontent>
    }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The Vufind Solr Recrod
   * @return tag scopecontent: get each field 520 with $a, subfields 3, a, b and concat according to pattern: $3: $a. $b ; write p tag for each field 520
   */
  def getScopeContentAbstract(record: VuFindSolrRecord): NodeSeq = {
    val notes = record.getFieldsAsMap("520")

    val scopecontent = new NodeBuffer

    {for (note <- notes) yield {
      if (note.contains("a")) {
        scopecontent +=
          <p>{
            (if (note.contains("3")) note.getOrElse("3", "") + ": " else "") +
              note.getOrElse("a", "") +
              (if (note.contains("b")) ". " + note.getOrElse("b", "") else "")
            }</p>
      }
      else {NodeSeq.Empty}
    }
    }

    if (scopecontent.nonEmpty) {
      <scopecontent><head>Inhaltsangabe</head>{scopecontent}</scopecontent>
    }
    else {NodeSeq.Empty}
    
  }

  /**
   * @param record The Vufind Solr Recrod
   * @return tag userestrict: get each field 506, subfields a, c and concat according to pattern: $a. $c; write p tag for each field 506
   */
  def getUserestrict(record: VuFindSolrRecord): NodeSeq = {
    val restrictions = record.getFieldsAsMap("506")

    val userestrict = new NodeBuffer

    {for (restriction <- restrictions) yield {
      if (restriction.contains("a") || restriction.contains("c")) {
        userestrict +=
        <p>{restriction.getOrElse("a", "") +
          (if (restriction.contains("a") && restriction.contains("c")) ". " else "") +
          restriction.getOrElse("c", "")}</p>
      }
      else {NodeSeq.Empty}
    }
    }

    if (userestrict.nonEmpty) {
      <userestrict><head>Benutzungsbeschränkung</head>{userestrict}</userestrict>
    }
    else {NodeSeq.Empty}
  }

  /**
   * @param record The Vufind Solr Recrod
   * @return tag arrangement, generated from first 351 $a
   */
  def getArrangement(record: VuFindSolrRecord): NodeSeq = {
    val arrangement = record.getFirstSubfield("351", "a").getOrElse("")

    if (arrangement.nonEmpty) {
      <arrangement>
        <head>Ordnungszustand</head>
        <p>{arrangement}</p>
      </arrangement>
    }
    else {NodeSeq.Empty}
  }

  /**
   * @param record The Vufind Solr Recrod
   * @return tag bibliography: get each field 510, subfield a; write p tag for each field 510
   */
  def getBibliographyReference(record: VuFindSolrRecord): NodeSeq = {
    val references = record.getFieldsAsMap("510")

    val bibliography = new NodeBuffer

    {
      for (reference <- references) yield {
        if (reference.contains("a")) {
          bibliography +=
            <p>{reference.getOrElse("a", "")}</p>
        }
        else {NodeSeq.Empty}
      }
    }

    if (bibliography.nonEmpty) {
      <bibliography><head>Literaturhinweise</head>{bibliography}</bibliography>
      }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The Vufind Solr Recrod
   * @return tag bibliography: get each field 581, subfields a, 3 and concat according to pattern: $a (betr. $3) ; write p tag for each field 581
   */
  def getBibliographyPublication(record: VuFindSolrRecord): NodeSeq = {
    val publications = record.getFieldsAsMap("581")

    val bibliography = new NodeBuffer

    {for (publication <- publications) yield {
      if (publication.contains("a")) {
        bibliography +=
          <p>{publication.getOrElse("a", "") +
            (if (publication.contains("3")) " (betr. " + publication.getOrElse("3", "") + ")" else "")
            }</p>
      }
      else {NodeSeq.Empty}
    }
    }

    if (bibliography.nonEmpty) {
      <bibliography>
        <head>Literaturhinweise</head>
        {bibliography}
      </bibliography>
    }
    else {NodeSeq.Empty}
  }

  /**
   * @param record The Vufind Solr Record
   * @return tag acqinfo: get each field 541, subfields 3, c, a, d, e, f and concat according to pattern: $3: $c. Herkunft: $a. Datum: $d. Akz.-Nr.: $e. Eigentümer: $f ; write p tag for each field 541
   */
  def getAcqinfo(record: VuFindSolrRecord): NodeSeq = {
    val acqnotes = record.getFieldsAsMap("541")

    if (acqnotes.nonEmpty) {
      <acqinfo>
        <head>Akzession</head>
        {for (acqnote <- acqnotes) yield {
          <p>{
            ((if (acqnote.contains("3")) acqnote.getOrElse("3", "") + ": " else "") +
            (if (acqnote.contains("c")) acqnote.getOrElse("c", "") + ". " else "") +
            (if (acqnote.contains("a")) "Herkunft: " + acqnote.getOrElse("a", "") + ". " else "") +
            (if (acqnote.contains("d")) "Datum: " + acqnote.getOrElse("d", "") + ". " else "") +
            (if (acqnote.contains("e")) "Akz.-Nr.: " + acqnote.getOrElse("e", "") + ". " else "") +
            (if (acqnote.contains("f")) "Eigentümer: " + acqnote.getOrElse("f", "") else "")).trim
          }</p>
      }
        }
      </acqinfo>

    }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The VuFind Solr Record
   * @return tag relatedmaterial: get each field 544, subfield n; write p tag for each field 544
   */
  def getRelatedMaterial(record: VuFindSolrRecord): NodeSeq = {
    val notes = record.getFieldsAsMap("544")

    val relatedmaterial = new NodeBuffer

    {
      for (note <- notes) yield {
        if (note.contains("n")) {
          relatedmaterial +=
          <p>{note.getOrElse("n", "")}</p>
        }
        else {NodeSeq.Empty}
      }
    }

    if (relatedmaterial.nonEmpty) {
      <relatedmaterial><head>Verwandte Verzeichnungseinheit</head>{relatedmaterial}</relatedmaterial>
    }
    else {NodeSeq.Empty}
  }

  /**
   * @param record The VuFind Solr Record
   * @return tag bioghist: get each field 545, subfields a, b and concat according to pattern: $a. $b ; write p tag for each field
   */
  def getBiogHist(record: VuFindSolrRecord): NodeSeq = {
    val notes = record.getFieldsAsMap("545")

    val bioghist = new NodeBuffer

    {
      for (note <- notes) yield {
        if (note.contains("a") || note.contains("b")) {
          bioghist +=
              <p>{note.getOrElse("a", "") +
              (if (note.contains("a") && note.contains("b")) ". " else "") +
              note.getOrElse("b", "")}</p>
        }
        else {
          NodeSeq.Empty
        }
      }
    }

    if (bioghist.nonEmpty) {
      <bioghist><head>Biographische Notiz</head>{bioghist}</bioghist>
    }
    else {NodeSeq.Empty}
  }

  /**
   * @param record The VuFind Solr Record
   * @return tag otherfindaid: get each field 555, subfield a; write p tag for each field 555
   */
  def getOtherFindaid(record: VuFindSolrRecord): NodeSeq = {
    val notes = record.getFieldsAsMap("555")

    val otherfindaid = new NodeBuffer

    {
      for (note <- notes) yield {
        if (note.contains("a")) {
          otherfindaid +=
            <p>{note.getOrElse("a", "")}</p>
        }
        else {NodeSeq.Empty}
      }
    }

    if (otherfindaid.nonEmpty) {
      <otherfindaid><head>Weitere Findmittel</head>{otherfindaid}</otherfindaid>
    }
    else {NodeSeq.Empty}
  }


  /**
   * @param record The VuFind Solr Record
   * @return tag custodhist: get each field 561, subfield a; write p tag for each field 561
   */
  def getCustodHist(record: VuFindSolrRecord): NodeSeq = {
    val notes = record.getFieldsAsMap("561")

    val custodhist = new NodeBuffer

    {
      for (note <- notes) yield {
        if (note.contains("a")) {
          custodhist +=
            <p>{note.getOrElse("a", "")}</p>
        }
        else {NodeSeq.Empty}
      }
    }

    if (custodhist.nonEmpty) {
      <custodhist><head>Angaben zur Herkunft</head>{custodhist}</custodhist>
    }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The VuFind Solr Record
   * @return tag origination generated from 100/700 where $4 = cre (Aktenbildner)
   */
  def getOriginationPerson(record: VuFindSolrRecord): NodeSeq = {
    val persons = record.getFieldsAsMap("100") ++ record.getFieldsAsMap("700")

    {origination(persons, "pers")}
  }

  /**
   * @param record The VuFind Solr Record
   * @return tag origination generated from 110/710 where $4 = cre (Aktenbildner)
   */
  def getOriginationCorporate(record: VuFindSolrRecord): NodeSeq = {
    val corporates = record.getFieldsAsMap("110") ++ record.getFieldsAsMap("710")

    {origination(corporates, "corp")}
  }

  /**
   * @param record The VuFind Solr Record
   * @return tag origination generated from 111/711 where $4 = cre (Aktenbildner)
   */
  def getOriginationConference(record: VuFindSolrRecord): NodeSeq = {
    val conferences = record.getFieldsAsMap("111") ++ record.getFieldsAsMap("711")

    {origination(conferences, "conf")}
  }

  def origination(creators: Seq[Map[String, String]], persOrCorpOrConf: String): NodeSeq = {
    val origination = new NodeBuffer

    for (creator <- creators) yield {
      if (creator.getOrElse("4", "") == "cre") {
        origination +=
          <origination>
            {
            if (persOrCorpOrConf == "pers") {
              {persname(creator, "cre")}
            }
            else if (persOrCorpOrConf == "corp") {
              {corpname(creator, "cre", "b")}
            }
            else if (persOrCorpOrConf == "conf") {
              {corpname(creator, "cre", "e")}
            }
            }
          </origination>
      }
    }

    if (origination.nonEmpty) {
      origination
    }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The VuFind Solr Record
   * @return tag controlacccess for persons from x00
   */
  def getPersonControlAccess(record: VuFindSolrRecord): NodeSeq = {
    val persons = record.getFieldsAsMap("100") ++ record.getFieldsAsMap("700")
    val subPersons = record.getFieldsAsMap("600")

    val controlAccess = new NodeBuffer

    if (persons.nonEmpty || subPersons.nonEmpty) {
          {for (person <- persons) yield {
          if (person.getOrElse("4", "") != "cre") {
            controlAccess +=
            {persname(person, person.getOrElse("4", ""))}
          }
          else {NodeSeq.Empty}
        }
          }
          {for (subPerson <- subPersons) yield {
          if (subPerson.getOrElse("c", "").contains("Familie")) {
            controlAccess +=
            persname(subPerson, "subF")
          }
          else {
            controlAccess +=
            persname(subPerson, "subP")
          }
        }
          }
    }

    if (controlAccess.nonEmpty) {
      <controlaccess><head>Personen</head>{controlAccess}</controlaccess>
    }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The VuFind Solr Record
   * @return tag controlaccess for corporates from fields x10, x11
   */
  def getCorporateControlAccess(record: VuFindSolrRecord): NodeSeq = {
    val corporates = record.getFieldsAsMap("110") ++ record.getFieldsAsMap("710")
    val subCorporates = record.getFieldsAsMap("610")
    val conferences = record.getFieldsAsMap("111") ++ record.getFieldsAsMap("711")
    val subConferences = record.getFieldsAsMap("611")

    val controlAccess = new NodeBuffer

    if (corporates.nonEmpty || subCorporates.nonEmpty || conferences.nonEmpty || subConferences.nonEmpty) {
          {for (corporate <- corporates) yield {
          if (corporate.getOrElse("4", "") != "cre") {
            controlAccess +=
            {corpname(corporate, corporate.getOrElse("4", ""), "b")}
          }
          else {NodeSeq.Empty}
        }
          }
          {for (subCorporate <- subCorporates) yield {
          controlAccess +=
          corpname(subCorporate, "subK", "b")
        }
          }
          {for (conference <- conferences) yield {
          if (conference.getOrElse("4", "") != "cre") {
            controlAccess +=
            {corpname(conference, conference.getOrElse("4", ""), "e")}
          }
          else {NodeSeq.Empty}
        }
          }
          {for (subConference <- subConferences) yield {
          controlAccess +=
          corpname(subConference, "subK", "e")
        }
          }
    }

    if (controlAccess.nonEmpty) {
      <controlaccess><head>Körperschaften</head>{controlAccess}</controlaccess>
    }
    else {NodeSeq.Empty}

  }

  /**
   * @param record The VuFind Solr Record
   * @return tag controlacces from field 650
   */
  def getSubjectControlAccess(record: VuFindSolrRecord): NodeSeq = {
    val subjects = record.getFieldsAsMap("650")

    if (subjects.nonEmpty) {
        <controlaccess>
          <head>Sachschlagwörter</head>
          {for (subject <- subjects) yield {
          subjectTag(subject)
        }
          }
        </controlaccess>
    }
    else {NodeSeq.Empty}
  }

  /**
   * @param record The VuFind Solr Record
   * @return tag controlaccess for genre from 655 $a
   */
  def getGenreControlAccess(record: VuFindSolrRecord): NodeSeq = {
    val genres = record.getFieldsAsMap("655")

    if (genres.nonEmpty) {
      <controlaccess>
        <head>Gattungen</head>
        {for (genre <- genres) yield {
        <genreform>{genre.getOrElse("a", "")}</genreform>
      }
        }
      </controlaccess>
    }
    else {NodeSeq.Empty}

  }

  /**
   * @param value a field 6xx
   * @return subject tag, concat $a -- $v -- $x -- $y -- $z
   */
  def subjectTag(value: Map[String, String]): Node = {
    val content: String =
      {
        (if (value.contains("a")) value.getOrElse("a", "") else "") +
          (if (value.contains("v")) " -- " + value.getOrElse("v", "") else "") +
          (if (value.contains("x")) " -- " + value.getOrElse("x", "") else "") +
          (if (value.contains("y")) " -- " + value.getOrElse("y", "") else "") +
          (if (value.contains("z")) " -- " + value.getOrElse("z", "") else "")
      }

    val gndId = {
      if (value.getOrElse("0", "").startsWith("(DE-588)")) {
        value.getOrElse("0", "").substring(8)
      }
      else ""
    }

    val tag = new Elem(null, "subject", scala.xml.Null, scala.xml.TopScope, false, Text(content))
    val attNormal = tag % Attribute(null, "normal", value.getOrElse("a", ""), Null)

    if (gndId.nonEmpty) {
      attNormal % Attribute(null, "authfilenumber", gndId, Attribute(null, "source", "GND", Null))
    }
    else {
      attNormal
    }

  }

  /**
   * @param record The VuFind Solr Record
   * @return controlaccess for places from fields 651, 751
   */
  def getPlaceControlAccess(record: VuFindSolrRecord): NodeSeq = {
    val places = record.getFieldsAsMap("751")
    val subPlaces = record.getFieldsAsMap("651")

    if (places.nonEmpty || subPlaces.nonEmpty) {
        <controlaccess>
          <head>Orte</head>
          {for (place <- places) yield {
          geogname(place, "Entstehungsort")
        }
          }
          {for (subPlace <- subPlaces) yield {
          geogname(subPlace, "Erwähnter Ort")
        }
          }
        </controlaccess>
    }
    else {NodeSeq.Empty}

  }

  def geogname(value: Map[String, String], role: String): Node = {
    val content: String =
    {
      (if (value.contains("a")) value.getOrElse("a", "") else "") +
        (if (value.contains("v")) " -- " + value.getOrElse("v", "") else "") +
        (if (value.contains("x")) " -- " + value.getOrElse("x", "") else "") +
        (if (value.contains("y")) " -- " + value.getOrElse("y", "") else "") +
        (if (value.contains("z")) " -- " + value.getOrElse("z", "") else "")
    }

    val gndId = {
      if (value.getOrElse("0", "").startsWith("(DE-588)")) {
        value.getOrElse("0", "").substring(8)
      }
      else ""
    }

    val tag = new Elem(null, "geogname", scala.xml.Null, scala.xml.TopScope, false, Text(content))
    val attNormal = tag % Attribute(null, "normal", value.getOrElse("a", ""), Attribute(null, "role", role, Null))

    if (gndId.nonEmpty) {
      attNormal % Attribute(null, "authfilenumber", gndId, Attribute(null, "source", "GND", Null))
    }
    else {
      attNormal
    }
  }

  /**
   * @param person a person from field x00
   * @return tag persname, generated from x00, concat subfields according to $a ($q) $b, $c ($d) -- $t. $n. $p. $l. $m. $r. $s. $o. $h
   */
  def persname(person: Map[String, String], role: String): Node = {

    val content: String =
      {
        (if (person.contains("a")) person.getOrElse("a", "") else "") +
          (if (person.contains("q")) " (" + person.getOrElse("q", "") + ")" else "") +
          (if (person.contains("b")) " " + person.getOrElse("b", "") else "") +
          (if (person.contains("c")) ", " + person.getOrElse("c", "") else "") +
          (if (person.contains("d")) " (" + person.getOrElse("d", "") + ")" else "") +
          (if (person.contains("t")) " -- " + person.getOrElse("t", "") else "") +
          (if (person.contains("n")) ". " + person.getOrElse("n", "") else "") +
          (if (person.contains("p")) ". " + person.getOrElse("p", "") else "") +
          (if (person.contains("l")) ". " + person.getOrElse("l", "") else "") +
          (if (person.contains("m")) ". " + person.getOrElse("m", "") else "") +
          (if (person.contains("r")) ". " + person.getOrElse("r", "") else "") +
          (if (person.contains("s")) ". " + person.getOrElse("s", "") else "") +
          (if (person.contains("o")) ". " + person.getOrElse("o", "") else "") +
          (if (person.contains("h")) ". " + person.getOrElse("h", "") else "")
      }

    val roleText = relator.getOrElse(role, "")

    val gndId = {
      if (person.getOrElse("0", "").startsWith("(DE-588)")) {
        person.getOrElse("0", "").substring(8)
      }
      else ""
    }

    val tag = new Elem(null, "persname", scala.xml.Null, scala.xml.TopScope, false, Text(content))
    val attNormal = tag % Attribute(null, "normal", person.getOrElse("a", ""), Null)

    if (gndId.nonEmpty && roleText.nonEmpty) {
      attNormal % Attribute(null, "role", roleText, Attribute(null, "authfilenumber", gndId, Attribute(null, "source", "GND", Null)))
    }
    else if (gndId.nonEmpty) {
      attNormal % Attribute(null, "authfilenumber", gndId, Attribute(null, "source", "GND", Null))
    }
    else if (roleText.nonEmpty) {
      attNormal % Attribute(null, "role", roleText, Null)
    }
    else {
      attNormal
    }
  }

  /**
   * @param corporate a corporate from field x10 or x11
   * @return tag corpname for a corporate body from x10 or x11, concat subfields according to $a. $b ($g) for x10, $a. $e ($g) for x11
   */
  def corpname(corporate: Map[String, String], role: String, subfieldSubUnit: String): Node = {

    val content: String =
    {
      (if (corporate.contains("a")) corporate.getOrElse("a", "") else "") +
        (if (corporate.contains(subfieldSubUnit)) ". " + corporate.getOrElse(subfieldSubUnit, "") else "") +
        (if (corporate.contains("g")) " (" + corporate.getOrElse("g", "") + ")" else "")
    }

    val roleText = relator.getOrElse(role, "")

    val gndId = {
      if (corporate.getOrElse("0", "").startsWith("(DE-588)")) {
        corporate.getOrElse("0", "").substring(8)
      }
      else ""
    }


    val tag = new Elem(null, "corpname", scala.xml.Null, scala.xml.TopScope, false, Text(content))
    val attNormal = tag % Attribute(null, "normal", corporate.getOrElse("a", ""), Null)

    if (gndId.nonEmpty && roleText.nonEmpty) {
      attNormal % Attribute(null, "role", roleText, Attribute(null, "authfilenumber", gndId, Attribute(null, "source", "GND", Null)))
    }
    else if (gndId.nonEmpty) {
      attNormal % Attribute(null, "authfilenumber", gndId, Attribute(null, "source", "GND", Null))
    }
    else if (roleText.nonEmpty) {
      attNormal % Attribute(null, "role", roleText, Null)
    }
    else {
      attNormal
    }
  }

  /***********/
  /* HELPERS */
  /***********/


  /**
   * Generates a simple xml element, based on marc fields/subfields
   * for example simpleTag(record, 852j, unitid) returns something like
   * <unitid>UBH NL 136 : 2 f</unitid>
   * Only the first occurence is used (for example only the first 852j is used, the others are ignored)
   *
   * @param record The Vufind Solr Recrod
   * @param marcField A string defining a marc subfield.*
   *                  "852j" -> means field 852, subfield j (4 characters)
   *                  "852A_j" -> means field 852, first indicator A, second indicator empty, subfield j (6 characters)
   * @param tagName The name of the resulting tag name
   * @return An xml element with name tagName
   */
  def simpleTag(record: VuFindSolrRecord, marcField: String, tagName: String): NodeSeq = {
    //only field and subfield
    if(marcField.length == 4) {
      val field = marcField.substring(0,3)
      val subfield = marcField.substring(3,4)
      val text = record.getFirstSubfield(field, subfield)
      text match {
        case None => NodeSeq.Empty
        case Some(value) => new Elem(null, tagName, scala.xml.Null, scala.xml.TopScope, false, Text(value))
      }
    } else if(marcField.length == 6) {
      val field = marcField.substring(0,3)
      val ind1 = marcField.substring(3,4).replace("_"," ")
      val ind2 = marcField.substring(4,5).replace("_"," ")
      val subfield = marcField.substring(5,6)
      val text = record.getFirstSubfield(field, subfield, ind1, ind2)
      text match {
        case None => NodeSeq.Empty
        case Some(value) => new Elem(null, tagName, scala.xml.Null, scala.xml.TopScope, false, Text(value))
      }

    }
    else {
      logger.error(marcField+" is an invalid field/subfield definition. Should have 4 digits (field+subfield) or 6 (field, indicator1, indicator2, subfield)")
      NodeSeq.Empty
    }
  }

  /**
   * Generates a simple xml element, based on marc fields/subfields. Concatenates all the occurences with a separator
   * for example for
   * <datafield tag="300" ind1=" " ind2=" ">
   *  <subfield code="a">Partitur (314 S.) ; 42 cm</subfield>
   *  <subfield code="a">Chorpartitur (56 Bl.) ; 30 cm</subfield>
   * </datafield>
   * simpleTagConcat(record, 300a, extent, ", ") returns something like
   * <extent>Partitur (314 S.) ; 42 cm, Chorpartitur (56 Bl.) ; 30 cm</extent>
   *
   * @param record    The Vufind Solr Recrod
   * @param marcField A string defining a marc subfield.*
   *                  "852j" -> means field 852, subfield j (4 characters)
   *                  "852A_j" -> means field 852, first indicator A, second indicator empty, subfield j (6 characters)
   * @param tagName   The name of the resulting tag name
   * @param separator The separator to use
   * @return An xml element with name tagName
   */
  def simpleTagConcat(record: VuFindSolrRecord, marcField: String, tagName: String, separator: String): NodeSeq = {
    //only field and subfield
    if(marcField.length == 4) {
      val field = marcField.substring(0,3)
      val subfield = marcField.substring(3,4)
      val text = record.getSubfields(field, subfield)
      text match {
        case None => NodeSeq.Empty
        case Some(value) => new Elem(null, tagName, scala.xml.Null, scala.xml.TopScope, false, Text(value.mkString(separator)))
      }
    }
    else if(marcField.length == 6) {
      val field = marcField.substring(0,3)
      val ind1 = marcField.substring(3,4).replace("_"," ")
      val ind2 = marcField.substring(4,5).replace("_"," ")
      val subfield = marcField.substring(5,6)
      val text = record.getSubfields(field, subfield, ind1, ind2)
      text match {
        case None => NodeSeq.Empty
        case Some(value) => new Elem(null, tagName, scala.xml.Null, scala.xml.TopScope, false, Text(value.mkString(separator)))
      }

    }
    else {
      logger.error(marcField+" is an invalid field/subfield definition. Should have 4 digits (field+subfield) or 6 (field, indicator1, indicator2, subfield)")
      NodeSeq.Empty
    }
  }

  /**
   * Same as simple tag but add an attribute to the xml
   * for example simpleTag(record, 852j, unitid, "foo", "bar") returns something like
   * <unitid foo ="bar">UBH NL 136 : 2 f</unitid>
   *

   * @param record The Vufind Solr Recrod
   * @param marcField A string defining a marc subfield.*
   *                  "852j" -> means field 852, subfield j (4 characters)
   *                  "852A_j" -> means field 852, first indicator A, second indicator empty, subfield j
   *
   * @param tagName The name of the resulting tag name
   * @param attributeName the name of an xml attribute
   * @param attributeValue the value of this attribute
   * @return
   */
  def simpleTagWithAttributes(record: VuFindSolrRecord, marcField: String, tagName: String, attributeName: String, attributeValue: String): NodeSeq = {
    val res = simpleTag(record, marcField, tagName)
    if (res == NodeSeq.Empty) {
      NodeSeq.Empty
    } else {
      val verbBody: RewriteRule = new RewriteRule {
        override def transform(n: Node): Seq[Node] = n match {
          case elem: Elem  => elem.copy(attributes = new UnprefixedAttribute(attributeName, attributeValue, Null))
          case n => n
        }
      }
      new RuleTransformer(verbBody).transform(res).head
    }
  }

  /**
   * Same as simple tag concat but add an attribute to the xml*
   * for example for
   * <datafield tag="300" ind1=" " ind2=" ">
   * <subfield code="a">Partitur (314 S.) ; 42 cm</subfield>
   * <subfield code="a">Chorpartitur (56 Bl.) ; 30 cm</subfield>
   * </datafield>
   * simpleTagConcat(record, 300a, extent, "foo", "bar", ", ") returns something like
   * <extent foo="bar">Partitur (314 S.) ; 42 cm, Chorpartitur (56 Bl.) ; 30 cm</extent>
   *
   * @param record         The Vufind Solr Recrod
   * @param marcField      A string defining a marc subfield.*
   *                       "852j" -> means field 852, subfield j (4 characters)
   *                       "852A_j" -> means field 852, first indicator A, second indicator empty, subfield j
   * @param tagName        The name of the resulting tag name
   * @param attributeName  The name of an xml attribute
   * @param attributeValue The value of this attribute
   * @param separator      The separator to use
   * @return
   */
  def simpleTagWithAttributesConcat(record: VuFindSolrRecord, marcField: String, tagName: String, attributeName: String, attributeValue: String, separator: String): NodeSeq = {
    val res = simpleTagConcat(record, marcField, tagName, separator)
    if (res == NodeSeq.Empty) {
      NodeSeq.Empty
    } else {
      val verbBody: RewriteRule = new RewriteRule {
        override def transform(n: Node): Seq[Node] = n match {
          case elem: Elem  => elem.copy(attributes = new UnprefixedAttribute(attributeName, attributeValue, Null))
          case n => n
        }
      }
      new RuleTransformer(verbBody).transform(res).head
    }
  }

  def validateDate(date: String): Boolean = {
    val eadDate = raw"^(-?(0|1|2)([0-9]{3})(((01|02|03|04|05|06|07|08|09|10|11|12)((0[1-9])|((1|2)[0-9])|(3[0-1])))|-((01|02|03|04|05|06|07|08|09|10|11|12)(\-((0[1-9])|((1|2)[0-9])|(3[0-1])))?))?)(\/-?(0|1|2)([0-9]{3})(((01|02|03|04|05|06|07|08|09|10|11|12)((0[1-9])|((1|2)[0-9])|(3[0-1])))|-((01|02|03|04|05|06|07|08|09|10|11|12)(-((0[1-9])|((1|2)[0-9])|(3[0-1])))?))?)?".r

    date match {
      case eadDate(_*) => true
      case _ => false
    }
  }
}
