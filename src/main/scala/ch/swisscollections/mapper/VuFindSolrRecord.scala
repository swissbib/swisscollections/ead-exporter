package ch.swisscollections.mapper

import org.apache.solr.common.SolrDocument

import scala.xml.{Elem, NodeSeq, XML}

class VuFindSolrRecord(solrDocument: SolrDocument) extends SolrDocument(solrDocument: SolrDocument){

  def xmlFullRecord: Elem = XML.loadString(solrDocument.getFieldValue("fullrecord").toString)

  //returns a sequence of the corresponding subfields
  def getSubfields(field: String, subfield: String): Option[Seq[String]] = {
    val res = for {
      df <- xmlFullRecord \\ "datafield"
      if df \@ "tag" == field
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf.text
    if (res.nonEmpty) {
      Some(res)
    } else {
      None
    }
  }

  //returns the first subfield value
  //otherwise return emtpy string
  def getFirstSubfield(field: String, subfield: String): Option[String] = {
    val res = for {
      df <- xmlFullRecord \\ "datafield"
      if df \@ "tag" == field
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf.text
    if (res.nonEmpty) {
      Some(res.head)
    } else {
      None
    }
  }

  //returns a sequence of the corresponding subfields
  //returns an empty list if there are no matching subfields
  def getSubfields(field: String, subfield: String, ind1: String, ind2: String): Option[Seq[String]] = {
    val res = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind1" == ind1 && df \@ "ind2" == ind2)
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf.text
    if (res.nonEmpty) {
      Some(res)
    } else {
      None
    }
  }

  //returns the first subfield value
  //otherwise return emtpy string
  def getFirstSubfield(field: String, subfield: String, ind1: String, ind2: String): Option[String] = {
    val res = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind1" == ind1 && df \@ "ind2" == ind2)
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf.text
    if (res.nonEmpty) {
      Some(res.head)
    } else {
      None
    }
  }

  //returns the value of the first matching controlfield
  def getControlfield(controlfield: String): String = {
    val res = for {
      df <- xmlFullRecord \\ "controlfield"
      if df \@ "tag" == controlfield
    } yield df
    res.head.text
  }

  def getControlfieldOption(controlfield: String): Option[String] = {
    val res = for {
      df <- xmlFullRecord \\ "controlfield"
      if df \@ "tag" == controlfield
    } yield df.text
    res.headOption
  }

  /* all subfields values */
  def getFieldsAsList(field: String): Seq[List[(String, String)]] = {
    val fieldNodes = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field)
    } yield (convertFieldToList(df))
    fieldNodes
  }

  /* only one value (the last one) per subfield code */
  def getFieldsAsMap(field: String): Seq[Map[String, String]] = {
    val fieldNodes = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field)
    } yield convertFieldToMap(df)
    fieldNodes
  }

  /* only one value (the last one) per subfield code */
  def getFieldsAsMap(field: String, ind1: String): Seq[Map[String, String]] = {
    val fieldNodes = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind1" == ind1)
    } yield convertFieldToMap(df)
    fieldNodes
  }

  /* only one value (the last one) per subfield code */
  def getFieldsAsMapInd(field: String, ind2: String): Seq[Map[String, String]] = {
    val fieldNodes = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind2" == ind2)
    } yield convertFieldToMap(df)
    fieldNodes
  }

  //todo : currently if a field has twice the same subfield, only one value is retained !!!
  def convertFieldToMap(field: NodeSeq): Map[String, String] ={
    var res = Map[String,String]()
    for (sf <- field \\ "subfield") {
      res += (sf \@ "code" -> sf.text)
    }
    res
  }

  def convertFieldToList(field: NodeSeq): List[(String, String)] ={
    var res = List[(String,String)]()
    for (sf <- field \\ "subfield") {
      res = res :+ (sf \@ "code", sf.text)
    }
    res
  }
}
