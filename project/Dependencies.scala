/*
 * SOLR SearchDocs transformations
 * Copyright (C) 2022  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import sbt._

object Dependencies {
  lazy val kafkaV = "2.4.1"
  lazy val log4jV = "2.17.0"
  lazy val log4jscalaV  = "12.0"
  lazy val scalatestV = "3.1.2"
  lazy val solrjV = "8.6.3"
  lazy val snakeyamlV = "1.27"
  lazy val saxonV = "10.2"
  lazy val scalaxmlV = "2.3.0"
  lazy val jodaTimeV = "2.10.6"


  //lazy val cats = "org.typelevel" %% "cats-core" % "2.1.1"
  lazy val jodaTime = "joda-time" % "joda-time" % jodaTimeV
  lazy val kafkaStreams = "org.apache.kafka" %% "kafka-streams-scala" % kafkaV
  lazy val kafkaStreamsTestUtils = "org.apache.kafka" % "kafka-streams-test-utils" % kafkaV
  lazy val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jV
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jV
  lazy val log4jScala = "org.apache.logging.log4j" %% "log4j-api-scala" % log4jscalaV
  lazy val log4jSlf4j = "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4jV
  //lazy val rdfRioApi = "org.eclipse.rdf4j" % "rdf4j-rio-api" % "3.2.3"
  //lazy val rdfRioNtriples = "org.eclipse.rdf4j" % "rdf4j-rio-ntriples" % "3.2.3"
  //lazy val requests = "com.lihaoyi" %% "requests" % "0.5.1"
  lazy val scalatic = "org.scalactic" %% "scalactic" % scalatestV
  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalatestV
  //lazy val scalaUri = "io.lemonlabs" %% "scala-uri" % "2.2.3"
  //lazy val uPickle = "com.lihaoyi" %% "upickle" % "0.9.5"
  lazy val kafkaClients = "org.apache.kafka" % "kafka-clients" % kafkaV


  lazy val scala_xml = "org.scala-lang.modules" %% "scala-xml" % scalaxmlV
  lazy val saxon = "net.sf.saxon" % "Saxon-HE" % saxonV
  lazy val snakeyaml = "org.yaml" % "snakeyaml" % snakeyamlV
  lazy val solrj = "org.apache.solr" % "solr-solrj" % solrjV
  lazy val courier =  "com.github.daddykotex" %% "courier" % "3.2.0"



}
