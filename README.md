# EAD Exporter

Export archival data from swisscollections for Kalliope and Archivportal Europa

Transform swisscollections marc to EAD for [Kalliope](https://kalliope-verbund.info/) and [Archivportal Europa](https://www.archivesportaleurope.net).

Get the data from swisscollections SOLR index and write it to xml dump files (one file pro Archive).

[Documentation](https://ub-basel.atlassian.net/wiki/spaces/RH/pages/1971585025/EAD+Exporter)


## Configuration

The configuration is provided via environmental variables. All environment variables have default values, see AppSettings.scala for details.

It is strongly recommended that you define the following variables : 

* `NODE_URL` Url of the solr server
* `SOLR_COLLECTION` Name of the solr collection where the records are located 

The following environment variables are optional, they have default values but this can be overwritten via environment variable.

* `CONNECTION_TIMEOUT` Solr connection timeout
* `SOCKET_TIMEOUT` Solr socket timeout
* `EXPORT_FOLDER` Path where the exported EAD files are located with a / at the end.
* `TEMP_FOLDER` Path where every archive file is written, before being zipped together
* `MAX_CHILD_RECORDS` Maximal number of child records (at a given level) that we retrieve. The other records will be ignored
* `MAX_ARCHIVES_PRO_INSTITUTION` Maximal number of to archive records that we retrieve for a given institution. If an institution have more archives (understand it as a "Nachlass"), the others will be ignored
* `MAX_PSEUDO_ARCHIVES_PRO_INSTITUTION` Maximal number of documents in a pseudo archive
* `MAIL_SERVER` Url of the smtp mail server to send the notification email 
* `MAIL_SERVER_PORT` Mail server port
* `EMAIL_ACCOUNT` Email account, usually an email address (or username)
* `EMAIL_PASSWORD` Email password for the `EMAIL_ACCOUNT`
* `MAIL_FROM` Sender of the email
* `MAIL_TO` Recipient of the mail 
* `MAIL_CC` CC Recipient of the mail
* `DOWNLOAD_URL` Url where the exported EAD files will be, it is used in the notification email
* `EAD_XSD_LOCATION` Folder containing the ead.xsd file
* `EXPORT_TYPE` either `kalliope` or `ape` (for Archivportal Europa). Defines the type of export to do

See in AppSettings.scala for the description of these environment variables

## Run tests

Some tests require a solr connection (for example to swisscollections test index). 

On gitlab CI, we only run tests which don't require the solr connection
`sbt 'testOnly -- -l ch.swisscollections.tags.NeedsSolrConnection'`

To run all tests
`sbt test`


